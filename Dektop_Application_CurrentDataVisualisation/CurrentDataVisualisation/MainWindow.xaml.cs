﻿using CurrentDataVisualisation.BindingConverter;
using CurrentMeasurements;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using unirest_net.http;

namespace CurrentDataVisualisation
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        public CurrentMeasurementsList Measurements { get; set; }

        public SQLiteConnection Conn;

        string StorageLocation = "";

        DateTime LastPlot = DateTime.MinValue;

        DispatcherTimer WebHookFetchTimer = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();

            Measurements = new CurrentMeasurementsList();

            dgOutput.DataContext = Measurements;

            StorageLocation = Properties.Settings.Default.LastStoragePath;

            resizedTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
            resizedTimer.Tick += ResizedTimer_Tick;

            SQLiteCommand cmd;

            if (StorageLocation == "" || !File.Exists(StorageLocation))
            {
                FileDialog sfd = new SaveFileDialog();
                while (true)
                {
                    try
                    {
                        switch(MessageBox.Show("Must select a valid file/path to save/open to/from!\nYes to open a file, No to save to a new location.","Open?",MessageBoxButton.YesNoCancel))
                        {
                            case MessageBoxResult.Yes:
                                sfd = new OpenFileDialog();
                                sfd.Filter = "SQLite-Datenbank|*.sqlite";
                                break;

                            case MessageBoxResult.No:
                                sfd = new SaveFileDialog();
                                sfd.Filter = "SQLite-Datenbank|*.sqlite";
                                break;

                            default:
                                Application.Current.Shutdown();
                                break;

                        }

                        sfd.ShowDialog();

                        StorageLocation = sfd.FileName;
                        break;
                    }
                    catch
                    {
                    }
                }

                Conn = CreateSQLiteConnection(StorageLocation);
                cmd = new SQLiteCommand(Conn);
                 
                if(sfd.GetType() == new SaveFileDialog().GetType()) 
                {
                    CurrentMeasurementsList.CreateNewDatabaseTable(cmd);
                }
                else
                {
                    Measurements.TryOpenFromNewLocation(cmd);
                }
            }
            else
            {
                Conn = CreateSQLiteConnection(StorageLocation);
                cmd = new SQLiteCommand(Conn);
                Measurements.TryOpenFromNewLocation(cmd);
            }
            try
            {
                Measurements.GetNewStorageDataFromTheThingsNetworkAndSaveToDatabase(cmd);
                Measurements.FetchLatestWebHookDataAndSave(cmd);
                WebHookDeleteAllData();
            }
            catch
            {
                MessageBox.Show("Can not fetch any data! No API-Connection possible.");
            }


            WebHookFetchTimer.Interval = new TimeSpan(0, 0, 10);
            WebHookFetchTimer.Tick += Timer_Tick;

            WebHookFetchTimer.Start();

            datePickerFrom.Text = (DateTime.Now - new TimeSpan(0, 30, 0)).ToString();
            datePickerTo.Text = DateTime.Now.ToString();

            MenuItem_Click_2(null, null);
        }

        private void ResizedTimer_Tick(object sender, EventArgs e)
        {
            plotUpdate(null, new RoutedEventArgs(null));

            resizedTimer.Stop();
        }

        public static void WebHookDeleteAllData()
        {
           Unirest.delete("https://webhook.site/token/a22ce7b7-2634-41be-9860-988ec38432b7/request")
            .header("Accept", "application/json")
            //   .header("date_from","2022-07-26 12:38:00")
            //  .header("date_to","2022-07-26 12:39:00")
            .asJson<string>();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (StorageLocation != "" && File.Exists(StorageLocation) && Conn != null)
                {
                    SQLiteCommand cmd = new SQLiteCommand(Conn);

                    Measurements.FetchLatestWebHookDataAndSave(cmd);
                    WebHookDeleteAllData();
                }
            }
            catch
            {
                MessageBox.Show("Can not fetch any data! No API-Connection possible.");
            }

            plotUpdate(null, new RoutedEventArgs());
        }
        public SQLiteConnection CreateSQLiteConnection(string fileName)
        {
            SQLiteConnection conn = new SQLiteConnection($"Data Source={fileName}");

            try
            {
                conn.Open();
            }
            catch (Exception exc)
            {
                throw exc;
            }

            return conn;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Conn != null)
                Conn.Close();

            Properties.Settings.Default.LastStoragePath = StorageLocation;
            Properties.Settings.Default.Save();
        }

        private void FetchTTNData()
        {
            if (Conn == null)
            {
                MessageBox.Show("Please select a file first!");
                return;
            }
            SQLiteCommand cmd = new SQLiteCommand()
            {
                Connection = Conn
            };

            Measurements.GetNewStorageDataFromTheThingsNetworkAndSaveToDatabase(cmd);
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "SQLite-Datbase|*.sqlite";

            ofd.ShowDialog();
            if (ofd.CheckFileExists)
            {
                try
                {
                    if (Conn != null)
                        Conn.Close();

                    Conn = CreateSQLiteConnection(ofd.FileName);

                    SQLiteCommand cmd = new SQLiteCommand(Conn);

                    if (Measurements.TryOpenFromNewLocation(cmd))
                    {
                        FetchTTNData();
                        Measurements.FetchLatestWebHookDataAndSave(cmd);
                        WebHookDeleteAllData();
                        plotUpdate(null, new RoutedEventArgs());
                    }
                    else
                    {
                        throw new Exception();
                    }

                    StorageLocation = ofd.FileName;
                }
                catch
                {
                    MessageBox.Show("Something went wrong. Select a valid file!");
                }
            }
            else
            {
                MessageBox.Show("No file selected.");
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = ".sqlite";
            sfd.Filter = "SQLite-Database|*.sqlite|CSV-File|*.csv";

            sfd.ShowDialog();
            try
            {
                switch (sfd.FileName.ToLower().Split('.')[sfd.FileName.ToLower().Split('.').Length - 1])
                {
                    case "csv":
                        using(StreamWriter sw = new StreamWriter(sfd.FileName)){ 
                            sw.WriteLine("Time;Current;Durration;Temperature;Humidity;Points for RMS");

                            foreach(CurrentMeasurementPoint point in Measurements.Items)
                            {
                                sw.WriteLine($"{point.Stop};{point.RmsVal};{point.Durration};{point.Temperature};{point.Humidity};{point.PointCnt}");
                            }

                            sw.Close();
                        }
                        break;

                    default:
                        if (Conn != null)
                            Conn.Close();

                        Conn = CreateSQLiteConnection(sfd.FileName);

                        SQLiteCommand cmd = new SQLiteCommand(Conn);

                        CurrentMeasurementsList.CreateNewDatabaseTable(cmd);

                        StorageLocation = sfd.FileName;

                        Measurements.SaveToNewDatabase(cmd);
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Something went wrong. Select a valid path!");
            }
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            Measurements.Items.Clear();
            MenuItem_Click_1(sender, e);
        }

        private void plotUpdate(object sender, RoutedEventArgs e)
        {
            if(tabControl.SelectedIndex == 0 && windowMainWindow.IsActive) 
            { 
            if (DateTime.Now - LastPlot >= new TimeSpan(0, 0, 0, 500) || sender == null)
            {
                if (!windowMainWindow.IsInitialized || cnvsPlot.ActualWidth == 0)
                    return;

                const double plotMargin = 20;
                double plotHeight = cnvsGrid.ActualHeight - plotMargin;
                double plotWidth = cnvsGrid.ActualWidth - plotMargin * 2;

                DateTime lastDate = DateTime.Now;
                DateTime firstDate;

                switch (cbPlottingTime.SelectedIndex)
                {
                    default:
                        try
                        {
                            firstDate = DateTime.Parse(datePickerFrom.Text);
                            lastDate = DateTime.Parse(datePickerTo.Text);

                            if (lastDate - firstDate < new TimeSpan(0, 5, 0))
                            {
                                MessageBox.Show("To must at least be 5 minutes after from!");
                                throw new ArgumentOutOfRangeException();
                            }
                        }
                        catch
                        {
                            MessageBox.Show("Enter valid values!");
                            return;
                        }
                        break;

                    case 0:
                        firstDate = DateTime.Now - new TimeSpan(1, 0, 0);
                        break;

                    case 1:
                        firstDate = DateTime.Now - new TimeSpan(6, 0, 0);
                        break;

                    case 2:
                        firstDate = DateTime.Now - new TimeSpan(12, 0, 0);
                        break;

                    case 3:
                        firstDate = DateTime.Now - new TimeSpan(24, 0, 0);
                        break;
                    case 4:
                        firstDate = DateTime.Now - new TimeSpan(7, 0, 0, 0);
                        break;

                    case 5:
                        firstDate = Measurements.Items[0].Stop;
                        break;
                }

                for (int i = 0; i < cnvsPlot.Children.Count; i++)
                {
                    if (cnvsPlot.Children[i] as Polyline == null)
                    {
                        cnvsPlot.Children.RemoveAt(i);
                        i--;
                    }
                }

                polyLineCurrent.Points.Clear();
                polyLineTemperature.Points.Clear();
                polyLineHumidity.Points.Clear();

                double minCurrent = double.MaxValue;
                double maxCurrent = double.MinValue;

                double minTemperature = double.MaxValue;
                double maxTemperature = double.MinValue;

                double minHumidity = double.MaxValue;
                double maxHumidity = double.MinValue;

                for (int i = Measurements.Items.Count - 1; i >= 0; i--)
                {
                    if (Measurements.Items[i].Stop <= firstDate)
                    {
                        break;
                    }else if (Measurements.Items[i].Stop > lastDate)
                        {
                            continue;
                        }

                    if (Measurements.Items[i].RmsVal < minCurrent)
                        minCurrent = Measurements.Items[i].RmsVal;
                    else if (Measurements.Items[i].RmsVal > maxCurrent)
                        maxCurrent = Measurements.Items[i].RmsVal;

                    if (Measurements.Items[i].Temperature < minTemperature)
                        minTemperature = Measurements.Items[i].Temperature;
                    else if (Measurements.Items[i].Temperature > maxTemperature)
                        maxTemperature = Measurements.Items[i].Temperature;

                    if (Measurements.Items[i].Temperature < minHumidity)
                        minHumidity = Measurements.Items[i].Humidity;
                    else if (Measurements.Items[i].Temperature > maxHumidity)
                        maxHumidity = Measurements.Items[i].Humidity;
                }

                if (minTemperature > 0)
                    minTemperature = 0;
                if (maxTemperature < 30)
                    maxTemperature = 30;
                if (minHumidity > 0)
                    minHumidity = 0;
                if (maxHumidity < 80)
                    maxHumidity = 80;
                if (maxCurrent - minCurrent <= 0.1)
                    maxCurrent = minCurrent + 0.1;

                maxCurrent *= 1.1;
                minCurrent *= 0.9;

                maxCurrent = Math.Round(maxCurrent, 1);
                minCurrent = Math.Round(minCurrent, 1);

                double prevCurMarkerX = double.MaxValue;
                double prevCurMarkerY = double.MaxValue;                

                double prevDataPointX = 0;
                

                double curSum = 0;
                int numberOfPlottetPoints = 0;

                int scaler = (int)sldScaler.Value;

                double powerEstimationSum = 0;
                double durrationSum = 0;

                for (int i = Measurements.Items.Count-scaler; i >= 0; i-=scaler)
                {
                    if (Measurements.Items[i].Stop < firstDate || i == 0)
                    {
                        tblNumberOfPlottetPoints.Text = numberOfPlottetPoints.ToString();
                        break;
                    }
                    else if (Measurements.Items[i].Stop > lastDate)
                    {
                        continue;
                    }

                    double rmsCurrent = 0;
                    int tmp = 0;
                    int hum = 0;
                    int overflowpoints = 0;

                    for(int a = 0; a < scaler; a++)
                    {
                            if (Measurements.Items[i+a].Stop <= lastDate) { 
                        rmsCurrent += Measurements.Items[i+a].RmsVal;
                        powerEstimationSum += (Measurements.Items[i + a].RmsVal * 230) * (Measurements.Items[i + a].Durration);
                        durrationSum += Measurements.Items[i + a].Durration;
                        curSum += Measurements.Items[i+a].RmsVal;
                        tmp += Measurements.Items[i+a].Temperature;
                        hum += Measurements.Items[i+a].Humidity;
                            }
                            else
                            {
                                overflowpoints++;
                            }
                        }

                    numberOfPlottetPoints++;

                    rmsCurrent = rmsCurrent/(scaler-overflowpoints);
                    tmp = (int)(tmp * 1.0 / (scaler-overflowpoints));
                    hum = (int)(hum * 1.0 / (scaler - overflowpoints));

                    Point curP = new Point();
                    Point tempP = new Point();
                    Point humP = new Point();

                    double x = plotWidth * ((Measurements.Items[i].Stop - firstDate).TotalSeconds * 1.0 / (lastDate - firstDate).TotalSeconds) + plotMargin;
                    if (i == Measurements.Items.Count - 1)
                        prevDataPointX = x;

                    curP.X = x;
                    tempP.X = x;
                    humP.X = x;

                    curP.Y = (plotHeight-plotMargin)* ((Math.Round(rmsCurrent, 3) - minCurrent) / (maxCurrent - minCurrent)) + plotMargin;

                    tempP.Y = (plotHeight-plotMargin) * (tmp - minTemperature) / (maxTemperature - minTemperature) + plotMargin;

                    humP.Y = (plotHeight-plotMargin) * 1.0 * (hum - minHumidity) / (maxHumidity - minHumidity) + plotMargin;

                    if(curP.Y < 2) { curP.Y = 2; }
                    if(humP.Y < 2) { humP.Y = 2; }
                    if(tempP.Y < 2) { tempP.Y = 2; }

             

                    if (prevCurMarkerX - curP.X >= 5 ||((prevCurMarkerY - curP.Y >= 5 || curP.Y - prevCurMarkerY >= 5) && prevCurMarkerX - curP.X >= 2))
                    {
                        Ellipse ell1 = new Ellipse();
                        Ellipse ell2 = new Ellipse();
                        Ellipse ell3 = new Ellipse();

                        ell1.SetBinding(Ellipse.VisibilityProperty, new Binding()
                        {
                            ElementName = "cbCurrent",
                            Path = new PropertyPath("IsChecked"),
                            Converter = new MyBoolToVisibilityConverter()
                        });
                        ell2.SetBinding(Ellipse.VisibilityProperty, new Binding()
                        {
                            ElementName = "cbTemperature",
                            Path = new PropertyPath("IsChecked"),
                            Converter = new MyBoolToVisibilityConverter()
                        });
                        ell3.SetBinding(Ellipse.VisibilityProperty, new Binding()
                        {
                            ElementName = "cbHumidity",
                            Path = new PropertyPath("IsChecked"),
                            Converter = new MyBoolToVisibilityConverter()
                        });

                        ell1.Fill = Brushes.Transparent;
                        ell2.Fill = Brushes.Transparent;
                        ell3.Fill = Brushes.Transparent;

             
                                ell1.Fill = Brushes.Red;
                                ell2.Fill = Brushes.Orange;
                                ell3.Fill = Brushes.AliceBlue;

                                prevCurMarkerX = x;
                            


                        ell1.Stroke = Brushes.Transparent;
                        ell2.Stroke = Brushes.Transparent;
                        ell3.Stroke = Brushes.Transparent;

                        ell1.Width = 10;
                        ell2.Width = 10;
                        ell3.Width = 10;

                        ell1.Height = 10;
                        ell2.Height = 10;
                        ell3.Height = 10;

                        ell1.ToolTip = $"{rmsCurrent:0.000} A\n @ {Measurements.Items[i].Stop:dd.MM HH:mm}\nClick to copy";
                        ell2.ToolTip = $"{tmp:00} °C\n @ {Measurements.Items[i].Stop:dd.MM HH:mm}\nClick to copy";
                        ell3.ToolTip = $"{hum:00} %\n @ {Measurements.Items[i].Stop:dd.MM HH:mm}\nClick to copy";

                        ell1.MouseDown += Ell_MouseDown;
                        ell2.MouseDown += Ell_MouseDown;
                        ell3.MouseDown += Ell_MouseDown;

                        prevDataPointX = x;
                        prevCurMarkerY = curP.Y;

                        Canvas.SetLeft(ell1, curP.X - 5);
                        Canvas.SetLeft(ell2, curP.X - 5);
                        Canvas.SetLeft(ell3, curP.X - 5);

                        Canvas.SetTop(ell1, curP.Y - 5);
                        Canvas.SetTop(ell2, tempP.Y - 5);
                        Canvas.SetTop(ell3, humP.Y - 5);

                        cnvsPlot.Children.Add(ell3);
                        cnvsPlot.Children.Add(ell2);
                        cnvsPlot.Children.Add(ell1);
                    }

                    polyLineCurrent.Points.Add(curP);
                    polyLineTemperature.Points.Add(tempP);
                    polyLineHumidity.Points.Add(humP);
                    }
                    LblCurScaleMax.Content = $"{maxCurrent:0.00} A";
                    LblCurScaleMid.Content = $"{(maxCurrent - minCurrent) / 2.0 + minCurrent:0.00} A";
                    LblCurScaleMin.Content = $"{minCurrent:0.00} A";

                    LblTempScaleMax.Content = $"{maxTemperature:00} °C";
                    LblTempScaleMid.Content = $"{(maxTemperature - minTemperature) / 2.0 + minTemperature:00} °C";
                    LblTempScaleMin.Content = $"{minTemperature:00} °C";

                    LblHumScaleMax.Content = $"{maxHumidity:00} %";
                    LblHumScaleMid.Content = $"{(maxHumidity - minHumidity) / 2.0 + minHumidity:00} %";
                    LblHumScaleMin.Content = $"{minHumidity:00} %";

                    lblMaxTime.Content = $"{lastDate:HH:mm}\n{lastDate:dd.MM}";
                    lblMidTime2.Content = $"{firstDate.AddSeconds((lastDate - firstDate).TotalSeconds * 2.0 / 3.0):HH:mm}\n{firstDate.AddSeconds((lastDate - firstDate).TotalSeconds * 2.0 / 3.0):dd.MM}";
                    lblMidTime1.Content = $"{firstDate.AddSeconds((lastDate - firstDate).TotalSeconds * 1.0 / 3.0):HH:mm}\n{firstDate.AddSeconds((lastDate - firstDate).TotalSeconds * 2.0 / 3.0):dd.MM}";
                    lblMinTime.Content = $"{firstDate:HH:mm}\n{firstDate:dd.MM}";

                LastPlot = DateTime.Now;

                double estimatedAverageWattHours = powerEstimationSum / durrationSum;
                
                if(estimatedAverageWattHours > 1000) 
                { 
                    tblEstimatedPowerUsage.Text = $"{estimatedAverageWattHours / 1000:0.0} kW";
                }
                else
                {
                    tblEstimatedPowerUsage.Text = $"{estimatedAverageWattHours:0} W";
                }
            }
            }
        }

        private void Ell_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Ellipse r = sender as Ellipse;
            Clipboard.SetText(r.ToolTip.ToString().Replace("Click to copy", ""));
        }

        private void plotUpdate(object sender, SelectionChangedEventArgs e)
        {
            plotUpdate(null, new RoutedEventArgs(null));
        }
        private void plotUpdate(object sender, MouseButtonEventArgs e)
        {
            plotUpdate(sender, new RoutedEventArgs(null));
        }

        DispatcherTimer resizedTimer = new DispatcherTimer();
        private void windowMainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            resizedTimer.Stop();
            resizedTimer.Start();
        }

        private void btnTransmit_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{\"downlinks\":[{\"decoded_payload\":{");
            sb.Append($"\"led1\":\"{cbLED1.IsChecked}\",");
            sb.Append($"\"led2\":\"{cbLED2.IsChecked}\",");
            sb.Append($"\"led3\":\"{cbLED3.IsChecked}\",");

            bool allOn = false;
            bool allOff = false;
            if (sender as Button != null)
            {
                Button tmp = sender as Button;
                if (tmp.Tag.ToString() == "alloff")
                {
                    allOff = true;
                }
                else if (tmp.Tag.ToString() == "allon")
                {
                    allOn = true;
                }
            }

            sb.Append($"\"allOff\":\"{allOff}\",");
            sb.Append($"\"allOn\":\"{allOn}\"");
            sb.Append("},\"f_port\":5");
            sb.Append("}]}");

            for (int i = 0; i < 1; i++)
            {
                PostData(sb.ToString());
            }
        }

        private void PostData(string body)
        {
            string key = "NNSXS.W7A5J4NQ4JUGED6B24WCXGVRONHQCTHGENJGCHA.FDPR4XRISCJEFY7M4ABZF3YXDYR5T4IPYGO4POKHGAE62CV47YDQ";

            Unirest.post($"https://eu1.cloud.thethings.network/api/v3/as/applications/testapp12349182/devices/eui-0080e115000a98f4/down/push")
                        .header("Content-Type:", "application/json")
                        .header("Authorization", $"Bearer {key}")
                        .body(body)
                       .asJson<string>();
        }

        private void btnNumberDownlink_Click(object sender, RoutedEventArgs e)
        {
            int number = 0;
            if (int.TryParse(tbxDownlinkANumber.Text, out number) && number >= -999 && number <= 9999)
            {
                
                StringBuilder sb = new StringBuilder();
                sb.Append("{\"downlinks\":[{\"decoded_payload\":{");
                if (number >= 0)
                {
                    sb.Append($"\"number4\":\"{number / 1000}\",");
                }
                else
                {
                    sb.Append($"\"number4\":\"16\","); //10 = minus char
                    number *= -1;
                }
                sb.Append($"\"mode\":\"{cbNumberDownlinkMode.SelectedIndex}\",");
                sb.Append($"\"number1\":\"{number % 10}\",");
                sb.Append($"\"number2\":\"{(number / 10) % 10}\",");
                sb.Append($"\"number3\":\"{(number / 100) % 10}\"");


                sb.Append("},\"f_port\":6");
                sb.Append("}]}");

                for (int i = 0; i < 1; i++)
                {
                     PostData(sb.ToString());
                }
            }
            else
            {
                MessageBox.Show("Enter a integer from 0 to 100!");
            }
        }

        private void sldScaler_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            resizedTimer.Stop();
            resizedTimer.Start();
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            notifyIcon.Visible = true;
            windowMainWindow.Hide();
            WebHookFetchTimer.Stop();
            WebHookFetchTimer.Interval = new TimeSpan(0, 0, (int)WebHookFetchTimer.Interval.TotalSeconds *10);
            WebHookFetchTimer.Start();
        }

        private void btnUplinkPeriodChange_Click(object sender, RoutedEventArgs e)
        {
            int period = 0;
            if(int.TryParse(tbxUplinkPeriod.Text,out period) && period > 9 && period < 7201)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("{\"downlinks\":[{\"decoded_payload\":{");
                sb.Append($"\"period\":\"{period}\"");

                sb.Append("},\"f_port\":7");
                sb.Append("}]}");

                for(int i = 0; i<1; i++) 
                {
                 PostData(sb.ToString());
                }

                WebHookFetchTimer.Interval = new TimeSpan(0, 0, period);
            }else
            {
                MessageBox.Show("Period must range from 10 to 7200 seconds!");
            }
        }

        private System.Windows.Forms.NotifyIcon notifyIcon = null;

        private void windowMainWindow_Initialized(object sender, EventArgs e)
        {
            notifyIcon = new System.Windows.Forms.NotifyIcon();
            notifyIcon.DoubleClick += NotifyIcon_Click;
            notifyIcon.Icon = new System.Drawing.Icon(@"MyIcon.ico");
            notifyIcon.Visible = false;
        }

        private void NotifyIcon_Click(object sender, EventArgs e)
        {
            notifyIcon.Visible = false;
            windowMainWindow.Show();
            plotUpdate(null, new RoutedEventArgs(null));
            
            WebHookFetchTimer.Stop();
            WebHookFetchTimer.Interval = new TimeSpan(0, 0, (int)WebHookFetchTimer.Interval.TotalSeconds / 10);
            WebHookFetchTimer.Start();
        }

        private void btnDeleteCurrentPoint_Click(object sender, RoutedEventArgs e)
        {
            try 
            {
                CurrentMeasurementPoint item = (CurrentMeasurementPoint)((Button)sender).DataContext;

                if (MessageBox.Show($"Do you really want to delete this entry?", "Delete?", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    SQLiteCommand cmd = new SQLiteCommand(Conn);
                    Measurements.RemoveWithDatabaseEntry(Measurements.GetIndexOfValueMatch(item),cmd);
                }
            } catch { }
        }

        private void datePickerFrom_LostFocus(object sender, RoutedEventArgs e)
        {
            if (windowMainWindow.IsInitialized)
            {
                DateTime date;

                if (DateTime.TryParse(datePickerFrom.Text, out date) && DateTime.TryParse(datePickerTo.Text, out date))
                {
                    plotUpdate(null, new RoutedEventArgs(null));
                }
            }
        }
    }
}
