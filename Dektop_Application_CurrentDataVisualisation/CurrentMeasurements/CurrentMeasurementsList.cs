﻿using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unirest_net.http;


namespace CurrentMeasurements
{
    public class CurrentMeasurementsList
    {
        public CurrentMeasurementsList()
        {
            items = new ObservableCollection<CurrentMeasurementPoint>();
        }

        private ObservableCollection<CurrentMeasurementPoint> items;

        public ObservableCollection<CurrentMeasurementPoint> Items
        {
            get { return items; }
            set { items = value; }
        }

        public void GetNewStorageDataFromTheThingsNetworkAndSaveToDatabase(SQLiteCommand cmd)
        {
            try { 
            /* 
             * Access Storage API from The Things Network, not meant for polling live date, keeps last 7 days stored
            */
            string key = "NNSXS.W7A5J4NQ4JUGED6B24WCXGVRONHQCTHGENJGCHA.FDPR4XRISCJEFY7M4ABZF3YXDYR5T4IPYGO4POKHGAE62CV47YDQ";

            CurrentMeasurementPoint latestExisitingPoint = GetLatestMeasurement();
            DateTime utcTime = latestExisitingPoint.Stop.ToUniversalTime();

            HttpResponse<string> response = Unirest.get($"https://eu1.cloud.thethings.network/api/v3/as/applications/testapp12349182/packages/storage/uplink_message?after={utcTime:yyyy-MM-dd}T{utcTime:HH:mm:ss}Z")
                .header("Authorization", $"Bearer {key}")
               .asJson<string>();

            string web = response.Body.ToString();

            string[] tokens = web.Split(new [] {"{\"result\":" }, StringSplitOptions.RemoveEmptyEntries); //The word "Result" always seperates the entrances
            
            CurrentMeasurementsList tmpList = new CurrentMeasurementsList();
            for(int i = tokens.Length-1; i >= 0; i--)
            {
                try 
                { 
                    string payload = tokens[i].Split(new[] {"\"decoded_payload\":"}, StringSplitOptions.None)[1].Split('}')[0].Replace("{","").Replace("}","").Replace("\"","");
                    string dateTime = tokens[i].Split(new[] { "\"time\":\""}, StringSplitOptions.None)[1].Split('\\')[0]; //is: 2022-07-25T08:50:32.4901Z
                    
                    string[] date = dateTime.Split('T')[0].Split('-'); //[0] = year,[1] = month,[2] = day
                    string[] time = dateTime.Split('T')[1].Split(':'); //[0] = hour,[1] = minute,[2] = second.millsecondT

                    DateTime timestamp = new DateTime(int.Parse(date[0]), int.Parse(date[1]), int.Parse(date[2]),
                                                      int.Parse(time[0]), int.Parse(time[1]), int.Parse(time[2].Split('.')[0]));
 
                        CurrentMeasurementPoint newPoint = CurrentMeasurementPoint.ParseFromJSONString(payload, timestamp);
                    
                        if (newPoint.Stop <= latestExisitingPoint.Stop)
                        {
                            break;
                        }

                        tmpList.items.Add(newPoint);
                }
                catch
                {
                    break;
                }
            }

            for(int i = tmpList.items.Count-1; i>=0; i--)
            {
                tmpList.items[i].SavedInDatabase = true;
                items.Add(tmpList.items[i]);
                tmpList.items[i].SaveToDatabase(cmd);
            }
            }
            catch
            {
                throw new Exception("Could not connect to web! Need a internet Connection for this application to run!");   
            }
        }

        public void SaveToNewDatabase(SQLiteCommand cmd)
        {
            foreach (CurrentMeasurementPoint point in Items)
            {
                point.SaveToDatabase(cmd);
            }
        }

        public bool TryOpenFromNewLocation(SQLiteCommand cmd)
        {
            items.Clear();

            try 
            { 
                cmd.CommandText = "SELECT measID,rmsVal,pointCnt,durration,temperature,humidity,start FROM Measurements ORDER BY start";

                SQLiteDataReader reader = cmd.ExecuteReader();

                while (reader.Read()) 
                {
                    string s = reader.GetString(6).Replace("\"","");
                    CurrentMeasurementPoint point = new CurrentMeasurementPoint(reader.GetFloat(1), reader.GetInt32(2), DateTime.Parse(s), reader.GetInt32(4), reader.GetInt32(5), reader.GetInt32(3));
                    point.MeasID = reader.GetInt32(0);
                    point.SavedInDatabase = true; 
                    items.Add(point);
                }

                reader.Close();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public void RemoveWithDatabaseEntry(int i, SQLiteCommand cmd)
        {
            if (items[i].SavedInDatabase)
            {
                cmd.CommandText = $"DELETE FROM Measurements WHERE measID = {Items[i].MeasID}";
                cmd.ExecuteNonQuery();
            }

            items.RemoveAt(i);
        }

        public int GetIndexOfValueMatch(CurrentMeasurementPoint item)
        {
            for(int i = 0; i<items.Count; i++)
            {
                if (items[i].Stop == item.Stop &&
                    items[i].MeasID == item.MeasID &&
                    items[i].PointCnt == item.PointCnt &&
                    items[i].Durration == item.Durration &&
                    items[i].Temperature == item.Temperature &&
                    items[i].Humidity == item.Humidity
                    )
                {
                    return i;
                }
            }

            return -1;
        }

        public static void CreateNewDatabaseTable(SQLiteCommand cmd)
        {
            cmd.CommandText = "DROP TABLE IF EXISTS Measurements";
            cmd.ExecuteNonQuery();

            cmd.CommandText = "CREATE TABLE Measurements (measID INTEGER PRIMARY KEY AUTOINCREMENT, rmsVal FLOAT, start DATETIME, pointCnt INT, durration INT, temperature INT, humidity INT)";
            cmd.ExecuteNonQuery();
        }

        private CurrentMeasurementPoint GetLatestMeasurement()
        {
            if (items.Count == 0)
                return new CurrentMeasurementPoint(0, 0, DateTime.MinValue, 0,0, 0);
            else
                return items[items.Count - 1];
        }

        public float GetMaxCurrent()
        {
            float max = float.MinValue;
            foreach(CurrentMeasurementPoint point in items)
            {
                if (point.RmsVal > max)
                    max = point.RmsVal;
            }

            return max;
        }

        public float GetMinCurrent()
        {
            float min = float.MaxValue;
            foreach (CurrentMeasurementPoint point in items)
            {
                if (point.RmsVal < min)
                    min = point.RmsVal;
            }

            return min;
        }

        public string GetWebHookData()
        {
            HttpResponse<string> response = Unirest.get("https://webhook.site/token/a22ce7b7-2634-41be-9860-988ec38432b7/requests?sorting=newest")
           .header("Accept", "application/json")
           //    .header("per_page ", "15")
           .asJson<string>();

            return response.Body.ToString();
        } 
        public void FetchLatestWebHookDataAndSave(SQLiteCommand cmd)
        {
            try { 
            /* Webhook-Site Only allows storage of 500 Requests */
            CurrentMeasurementPoint latestSaveMeasurement = GetLatestMeasurement();

            string json = GetWebHookData();
            string[] tokens = json.Split(new[] { "\"content\":" }, StringSplitOptions.None);

            CurrentMeasurementsList tmpList = new CurrentMeasurementsList();
            for(int i = 1; i<tokens.Length; i++)
            {
                try { 
                string payload = tokens[i].Split(new[] { "\\\"decoded_payload\\\":" }, StringSplitOptions.None)[1].Split('}')[0].Replace("{", "").Replace("}", "").Replace("\"", "");

                string dateTime = tokens[i].Split(new[] { "\\\"time\\\":\\\"" }, StringSplitOptions.None)[1].Split('\\')[0]; //is: 2022-07-25T08:50:32.4901Z

                string[] date = dateTime.Split('T')[0].Split('-'); //[0] = year,[1] = month,[2] = day
                string[] time = dateTime.Split('T')[1].Split(':'); //[0] = hour,[1] = minute,[2] = second.millsecondT

                DateTime timestamp = new DateTime(int.Parse(date[0]), int.Parse(date[1]), int.Parse(date[2]),
                                                      int.Parse(time[0]), int.Parse(time[1]), int.Parse(time[2].Split('.')[0]));

                CurrentMeasurementPoint point = CurrentMeasurementPoint.ParseFromJSONString(payload, timestamp);

                if(point.Stop <= latestSaveMeasurement.Stop)
                {
                    break;
                }

                tmpList.items.Add(point);
                }
                catch
                {
                    
                }
            }

            for(int i = tmpList.items.Count-1; i>=0; i--)
            {
                tmpList.items[i].SavedInDatabase = true;
                items.Add(tmpList.items[i]);
                tmpList.items[i].SaveToDatabase(cmd);
            }
            }
            catch
            {
                throw new Exception("Could not connect to web! Need a internet Connection for this application to run!");
            }

        }
    }
}
