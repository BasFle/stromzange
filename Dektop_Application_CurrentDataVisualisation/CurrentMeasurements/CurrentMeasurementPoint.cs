﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CurrentMeasurements
{
    public class CurrentMeasurementPoint:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public CurrentMeasurementPoint(float rmsVal, int pointCnt, DateTime stop, int temperature,int humidity, int durration, bool savedInDatabase = false)
        {
            this.rmsVal = rmsVal;
            this.pointCnt = pointCnt;
            this.stop = stop;
            this.temperature = temperature;
            this.humidity = humidity;
            this.durration = durration;
            this.savedInDatabase = savedInDatabase;
        }

        private bool savedInDatabase;

        private int humidity;

        public int Humidity
        {
            get { return humidity; }
            set { humidity = value;
                OnPropertyChanged("Humidity");
            }
        }

        public bool SavedInDatabase
        {
            get { return savedInDatabase; }
            set { savedInDatabase = value; }
        }

        private int measID;

        private int pointCnt;

        private DateTime stop;

        private int temperature;

        private float rmsVal;

        private int durration;

        public int Durration
        {
            get { return durration; }
            set { durration = value;
                OnPropertyChanged("Durration");
            }
        }

        public float RmsVal
        {
            get { return rmsVal; }
            set { rmsVal = value;
                OnPropertyChanged("RmsVal");
            }
        }


        public int Temperature
        {
            get { return temperature; }
            set { temperature = value;
                OnPropertyChanged("Temperature");
            }
        }

        public DateTime Stop
        {
            get { return stop; }
            set { stop = value;
                OnPropertyChanged("Start");
            }
        }

        public int PointCnt
        {
            get { return pointCnt; }
            set { pointCnt = value;
                OnPropertyChanged("PointCnt");
            }
        }

        public int MeasID
        {
            get { return measID; }
            set { measID = value;
                OnPropertyChanged("MeasID");
            }
        }

        //s should be in format: "measDurration:10,measHumidity:50,measPointsForRMS:3410,measTemp:27,rmsCurrent:2.802596928649634e-45"
        //DateTime should hold the Timestamp of the message
        public static CurrentMeasurementPoint ParseFromJSONString(string s, DateTime time)
        {
            string[] tokens = s.Split(',');

            if(tokens.Length != 5)
            {
                throw new ArgumentException("Invalid arguemnt");
            }

            int measDurration = int.Parse(tokens[0].Split(':')[1]);
            int measPointsForRMS = int.Parse(tokens[2].Split(':')[1]);
            int temperature = int.Parse(tokens[3].Split(':')[1]);
            float rmsCurrent = float.Parse(tokens[4].Split(':')[1].Replace('.',','));
            int humidity = int.Parse(tokens[1].Split(':')[1]);

            CurrentMeasurementPoint point = new CurrentMeasurementPoint(rmsCurrent,measPointsForRMS, time.ToLocalTime(), temperature,humidity,measDurration,false);

            return point;
        }

        public void SaveToDatabase(SQLiteCommand cmd)
        {
            cmd.CommandText = "INSERT INTO Measurements (rmsVal,pointCnt,durration,temperature,humidity,start)" +
                            $"VALUES ({RmsVal.ToString().Replace(',','.')},{PointCnt},{Durration},{Temperature},{humidity},'{stop.ToString("yyyy - MM - dd HH: mm: ss")}')";
            cmd.ExecuteNonQuery();

            cmd.CommandText = $"SELECT last_insert_rowid()";  //gets last insert id
            MeasID = (int)((Int64)cmd.ExecuteScalar()); //Why like this? idk but works

            SavedInDatabase = true;
        }

        public override string ToString()
        {
            return $"At {stop:dd.MM.yy HH:mm:ss} for {durration} seconds: {rmsVal:0.00} A.\nBased on {pointCnt} points @ {temperature}°C / {humidity}%.";
        }
    }
}
