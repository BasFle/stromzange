#include "SevenSegment.h"
#include "stm32wlxx_hal.h"

uint8_t SevenSegmentDigits[] =
{
		63, //0
		6,	//1
		91,	//2
		79,	//3
		102,//4
		109,//...
		125,
		7,
		127,
		111,
		119,//A
		124,//b
		57, //C
		94, //d
		121,//E
		113,//F,
		64 //-
};

void WriteNumberToDisplay(int numb)
{
	uint8_t num = 0;
	if(numb < sizeof (SevenSegmentDigits)/sizeof(uint8_t))
	{
		//If no bit combination is defined for the number, the 7-segment display gets turned off (fallbackvalue for num is 0b0000 0000 = all leds off)
		num = SevenSegmentDigits[numb];
	}

	HAL_GPIO_WritePin(SegmentA_Port, SegmentA_Pin, num & 0x01);
	HAL_GPIO_WritePin(SegmentB_Port, SegmentB_Pin, num & 0x02);
	HAL_GPIO_WritePin(SegmentC_Port, SegmentC_Pin, num & 0x04);
	HAL_GPIO_WritePin(SegmentD_Port, SegmentD_Pin, num & 0x08);
	HAL_GPIO_WritePin(SegmentE_Port, SegmentE_Pin, num & 0x10);
	HAL_GPIO_WritePin(SegmentF_Port, SegmentF_Pin, num & 0x20);
	HAL_GPIO_WritePin(SegmentG_Port, SegmentG_Pin, num & 0x40);
	HAL_GPIO_WritePin(DecimalPoint_Port, DecimalPoint_Pin, 0);
}

void WriteNumberAndPointToDisplay(int numb)
{
	uint8_t num = 0;
	if(numb < sizeof (SevenSegmentDigits)/sizeof(uint8_t))
	{
		//If no bit combination is defined for the number, the 7-segment display gets turned off (fallbackvalue for num is 0b0000 0000 = all leds off)
		num = SevenSegmentDigits[numb];
	}

	HAL_GPIO_WritePin(SegmentA_Port, SegmentA_Pin, num & 0x01);
	HAL_GPIO_WritePin(SegmentB_Port, SegmentB_Pin, num & 0x02);
	HAL_GPIO_WritePin(SegmentC_Port, SegmentC_Pin, num & 0x04);
	HAL_GPIO_WritePin(SegmentD_Port, SegmentD_Pin, num & 0x08);
	HAL_GPIO_WritePin(SegmentE_Port, SegmentE_Pin, num & 0x10);
	HAL_GPIO_WritePin(SegmentF_Port, SegmentF_Pin, num & 0x20);
	HAL_GPIO_WritePin(SegmentG_Port, SegmentG_Pin, num & 0x40);
	HAL_GPIO_WritePin(DecimalPoint_Port, DecimalPoint_Pin, 1);
}
