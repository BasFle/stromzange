################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../DHT11/DHT.c 

OBJS += \
./DHT11/DHT.o 

C_DEPS += \
./DHT11/DHT.d 


# Each subdirectory must supply rules for building sources it contributes
DHT11/%.o DHT11/%.su: ../DHT11/%.c DHT11/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DCORE_CM4 -DSTM32WL55xx -DUSE_HAL_DRIVER -c -I../../Core/Inc -I"C:/Users/flba/Desktop/o/en.stm32cubewl_v1-2-0/STM32Cube_FW_WL_V1.2.0/Projects/NUCLEO-WL55JC/Applications/LoRaWAN/LoRaWAN_End_Node/STM32CubeIDE/SevenSegmentDisplay" -I"C:/Users/flba/Desktop/o/en.stm32cubewl_v1-2-0/STM32Cube_FW_WL_V1.2.0/Projects/NUCLEO-WL55JC/Applications/LoRaWAN/LoRaWAN_End_Node/STM32CubeIDE/DHT11" -I"C:/Users/flba/Desktop/o/en.stm32cubewl_v1-2-0/STM32Cube_FW_WL_V1.2.0/Projects/NUCLEO-WL55JC/Applications/LoRaWAN/LoRaWAN_End_Node/STM32CubeIDE/DS1307" -I../../LoRaWAN/App -I../../LoRaWAN/Target -I../../../../../../../Drivers/STM32WLxx_HAL_Driver/Inc -I../../../../../../../Drivers/STM32WLxx_HAL_Driver/Inc/Legacy -I../../../../../../../Utilities/trace/adv_trace -I../../../../../../../Utilities/misc -I../../../../../../../Utilities/sequencer -I../../../../../../../Utilities/timer -I../../../../../../../Utilities/lpm/tiny_lpm -I../../../../../../../Middlewares/Third_Party/LoRaWAN/LmHandler/Packages -I../../../../../../../Middlewares/Third_Party/SubGHz_Phy -I../../../../../../../Middlewares/Third_Party/SubGHz_Phy/stm32_radio_driver -I../../../../../../../Drivers/CMSIS/Device/ST/STM32WLxx/Include -I../../../../../../../Middlewares/Third_Party/LoRaWAN/Crypto -I../../../../../../../Middlewares/Third_Party/LoRaWAN/Mac/Region -I../../../../../../../Middlewares/Third_Party/LoRaWAN/Mac -I../../../../../../../Middlewares/Third_Party/LoRaWAN/LmHandler -I../../../../../../../Middlewares/Third_Party/LoRaWAN/Utilities -I../../../../../../../Drivers/CMSIS/Include -I../../../../../../../Drivers/BSP/STM32WLxx_Nucleo -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Drivers/STM32WLxx_HAL_Driver/Inc -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Drivers/STM32WLxx_HAL_Driver/Inc/Legacy -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Utilities/trace/adv_trace -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Utilities/misc -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Utilities/sequencer -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Utilities/timer -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Utilities/lpm/tiny_lpm -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Middlewares/Third_Party/LoRaWAN/LmHandler/Packages -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Middlewares/Third_Party/SubGHz_Phy -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Middlewares/Third_Party/SubGHz_Phy/stm32_radio_driver -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Drivers/CMSIS/Device/ST/STM32WLxx/Include -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Middlewares/Third_Party/LoRaWAN/Crypto -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Middlewares/Third_Party/LoRaWAN/Mac/Region -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Middlewares/Third_Party/LoRaWAN/Mac -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Middlewares/Third_Party/LoRaWAN/LmHandler -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Middlewares/Third_Party/LoRaWAN/Utilities -IC:/Users/flba/STM32Cube/Repository/STM32Cube_FW_WL_V1.2.0/Drivers/CMSIS/Include -Og -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-DHT11

clean-DHT11:
	-$(RM) ./DHT11/DHT.d ./DHT11/DHT.o ./DHT11/DHT.su

.PHONY: clean-DHT11

