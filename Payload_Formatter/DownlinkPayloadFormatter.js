function encodeDownlink(input)
{
  // Input is a string true or false
  var bytes = [];
  
  if(input.fPort == 5){
  bytes[0] = 0;
  
  if(input.data.led1 == "True")
  {
    bytes[0] |= 0x01;
  }
  
  if(input.data.led2 == "True")
  {
    bytes[0] |= 0x02;  
  }
  
  if(input.data.led3 == "True")
  {
    bytes[0] |= 0x04;  
  }
  
  if(input.data.allOff == "True"){
    bytes[0] |= 0x08;
  }
  
  if(input.data.allOn == "True"){
    bytes[0] |= 0x10;
  }
  }
  else if(input.fPort == 6)
  {
    bytes[0] = input.data.mode;
    bytes[1] = input.data.number1;
    bytes[2] = input.data.number2;
    bytes[3] = input.data.number3;
    bytes[4] = input.data.number4;
  }
  else if(input.fPort == 7)
  {
    bytes[1] = input.data.period&0xFF;
    bytes[0] = (input.data.period>>8)&0xFF;
  }
  
  return {
    bytes: bytes,
    fPort: input.fPort
  };
}

function decodeDownlink(input) {
  
  if(input.fPort == 5){
      return {
    data: 
    {
      led1: ["False", "True"][input.bytes[0]&0x01],
      led2: ["False", "True"][input.bytes[0]&0x02],
      led3: ["False", "True"][input.bytes[0]&0x04],
      allOff: ["False", "true"][input.bytes[0]&0x08],
      allOn: ["False", "true"][input.bytes[0]&0x10]
    }
    };
  }
  else if(input.fPort == 6)
  {
      return {
      data:
      {
        mode:input.bytes[0],
        number1:input.bytes[1],
        number2:input.bytes[2],
        number3:input.bytes[3],
        number4:input.bytes[4]
        }
    };
  }
  else if(input.fPort == 7){
    return{
      data:
      {
        period:((input.bytes[0]<<8)|input.bytes[1])
      }
    }
  }
}
