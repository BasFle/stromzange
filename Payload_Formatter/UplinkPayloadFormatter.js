function Decoder(bytes, port)
{
  var decoded = {};
  
decoded.measDurration = bytes[0]|(bytes[1]<<8);
decoded.measTemp = bytes[2];
decoded.measHumidity = bytes[3];

var rmsCurrent = decodeFloat32(bytes[7]| (bytes[6]<<8)|(bytes[5]<<16)|bytes[4]<<24);
decoded.rmsCurrent = rmsCurrent;

var measPointsForRMS = bytes[8]|(bytes[9]<<8)|(bytes[10]<<16)|(bytes[11]<<24);
decoded.measPointsForRMS = measPointsForRMS;

return decoded;
}

function decodeFloat32(bytes)
{
    var sign = (bytes & 0x80000000) ? -1 : 1;
    var exponent = ((bytes >> 23) & 0xFF) - 127;
    var significand = (bytes & ~(-1 << 23));

    if (exponent == 128)
        return sign * ((significand) ? Number.NaN : Number.POSITIVE_INFINITY);

    if (exponent == -127) {
        if (significand == 0) return sign * 0.0;
        exponent = -126;
        significand /= (1 << 22);
    } else significand = (significand | (1 << 23)) / (1 << 23);

    return sign * significand * Math.pow(2, exponent);
}


