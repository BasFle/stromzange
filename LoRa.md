#LoRa

Kurz die wichtigsten Dinge über LoRa zusammengefasst.

[TOC]

## Was ist LoRa?
LoRa steht für Long Range Wide Area. Es handelt sich um eine Technologie, welche es ermöglicht unter Anwendung von extrem wenig Energie über Reichweiten von mehreren Kilometern Daten zu funken. Es gibt Sensormodule, welche über mehrere Jahre hinweg auf Basis einer Batterie Messdaten übertragne können.

In der Literatur unterscheidet man zwischen LoRa und LoRaWAN. LoRa beschreibt die physikalische Technologie, welche die Übertragung bei geringstem Leistungsverbrauch ermöglicht.
LoRaWAN beschreibt ein Netzwerkprotokoll, welches von LoRa basierten Chips angewendet wird.

Den wahrscheinlich besten Einstieg und auch detailierter Informationen bildet folgendes Wiki: 
(The Things Network - LoRaWAN)[https://www.thethingsnetwork.org/docs/lorawan/]

## Vorteile
- Niedriger Energieverbrauch beim Senden
- Hohe Reichweite (bis 10km) und einfache Abdeckung von großen Flächen
- Günstig

## Nachteile
- Niedrige Datenrate (250 Bit/s - 11kBit/s)

## Anwedungen & Limitations
Die Anwendungen sind sehr vielfältig.
- Steuerungen die selten umgeschaltet werden müssen
- Sensoren mit jahrelanger Batterielaufzeit

Die Begrenzungen ergeben sich durch die geringen Datenraten:
- Keine großen Datenmengen (Bilder, ...)
- Keine Realtime Data (Übertragung nur im Minuten/mehrere Sekunden Takt)

## Beachten
- Möglichst geringe Datenmenge (binary encoding)
- Möglichst seltenes Senden

## Netzwerkstruktur
Interessant ist die Netzwerkstruktur von LoRa. Im Prinzip kann jeder Gateways erwerben und diese irgendwo, wo dieser mit dem Internet verbunden sind platzieren. Schaltet man sein Gateway auf Public, kann sich theoretisch jedes Gerät damit verbinden.
Kommen LoRa-Geräte in Reichweite von einem Gateway, verbinden sich diese damit und übertragen darüber die Daten. Das Gateway selbst bildet anschließend die Schnittstelle zum Internet und einer Website wie beispielsweise [The Things Network](https://www.thethingsnetwork.org/).

Der Vorteil ist, dass so extrem große Flächen für Geräte abgedeckt werden können, da einerseits die Reichweite so groß ist, und andererseits eben alle Gateways als Verbindungsstation infrage kommen können: [Karte von öffentlich verbundenen Gateways](https://www.thethingsnetwork.org/map)

## Device EUI, Join EUI, App Key
Diese 3-Parameter beschreiben im wesentlichen jedes LoRaWAN fähige Gerät.

DevEUI - Eindeutige 64-Bit Geräteadresse (wird von IEEE vergeben)
JoinEUI - Eindeutige 64-Bit Adresse, wleche dem Applikationsserver zugeordnet ist (auch hier IEEE vergeben)
AppKey - 128-Bit Verschlüsselung zwischen Nachricht und Applikationsserver, muss für jedes Gerät eindeutig sein

Stellt man diese Parameter alle richtig ein, kann man sein Gerät zu einer Applikation beispielsweise unter eben dem The Things Network hinzufügen. Über diese Daten können die Dinge, welche ein Gatway empfängt dann eben an die richtige Stelle weitergeleitet werden.

## LoRa Programmierung
Ein guter Einstieg in LoRa, der relativ anfängerfreundlich ist und gutes Verständnis bringen kann ist das Loris Base-Board. Es ist quasi Plug and Play und bringt Verständnis: [Loris Base.pdf](attachements/LorisBase.pdf)
Ein Nachteil des Boards ist, dass es einen ST-Link-Programmer benötigt, um programmiert werden zu können.

Ein weiteres Board, welches verwendet werden kann und über deutlich mehr Möglichkeiten verfügt ist das STM32WL55 Board. [How to build a lora application with STM32Cube](attachements/an5406-how-to-build-a-lora-application-with-stm32cubewl-stmicroelectronics.pdf)
