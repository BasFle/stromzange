# Stromzange - Ferialpraktikumsprojekt

[TOC]

## Vorgabe
Umgesetzt werden soll im Prinzip eine auf LoRa-Technologie basierende Stromzange. Die Daten sollen per Funkmodul auf das [The Things Network](https://eu1.cloud.thethings.network/console/applications/testapp12349182/data) hochgeladen werden.

Zusätzlich zur Stromzange sollen nach Möglichkeit noch weitere Sensoren an den µC angeschlossen werden. Auch diese Daten sollen mithilfe der LoRa Technologie auf das The Things Network geladen werden.

## Umsetzung
### Blockschaltbild
![](attachements/Pasted_image_20220801104155.png)

Oben dargestellt ist ein Blockschaltbild, des umgesetzten Systemes.

Wesentlich umgesetzt wurde dabei eine einfache Desktop-Applikation, welche Daten aus dem The Things Network bezieht, lokal anzeigt und in einer lokalen Datenbank abspeichert, sowie die Firmware für das STM32WL55, welches die Sensordaten einsammelt und an das Lora Netzwerk sendet.

Auf Seite des STM32 gibt es folgende Dinge:
  - RTC
  - Luftfeuchtigkeits & Temperatur Sensor
  - 4 stellige 7 Segment Anzeige
  - Stromzange

Am einfachsten nachvollziehen lässt sich die Funktion, wenn man von der Stromzange SCT013 ausgeht und den Weg der Daten von dort aus verfolgt.

### Stromzange
[Datenblatt der Stromzange SCT-013](attachements/SCT-013-000-XiDiTechnology.pdf)
Die Stromzange wird um den Leiter, dessen Strom gemessen werden soll geschlossen. So kann kontaktlos der durch den Leiter fließende Strom ermittelt werden. In unserem Fall handelt es sich beim Wechselstrom um Strom, welcher vom Netz stammt, daher also auf einer Frequenz von 50 Hz basiert. 

**Hinweis**
Zu beachten ist, dass nur Wechselströme gemessen werden können. Außerdem kann die Stromzange nicht um ein klassisches 3-Adriges Kabel geschlossen werden, da in diesem Fall die "Summer der Ströme" 0 A entspricht, da sich sowohl Phase, als auch Neutralleiter innerhalb der Stromzange befinden.

Daher müssen diese Leiter durch einen Adapter voneinander getrennt werden. Ein möglicher Aufbau hierzu ist nachfolgend dargestellt:
![](attachements/Pasted_image_20220728141557.png)

Die Stromzange liefert in diesem Fall einen Ausgangsstrom, welcher proportional zum durch die Zange fließenden Strom ist. 100 A entsprechen dabei einem Ausgangsstrom von 50 mA, also einem Übersetzungsverhältnis von 2000 : 1. Da nur Wechselstrom gemessen werden kann, ergibt sich auch am Ausgang der Stromzange ein Wechselstrom.

Dieser Wechselstrom muss nun mit dem Mikrocontroller (STM32WL55) aufgezeichnet werden.
Dazu kommt folgende Strom/Spannungswandler Schaltung zum Einsatz:

![](attachements/Pasted_image_20220728143816.png)

An den Anschlüssen Input 1 und Input 2 wird dabei die Stromzange angeschlossen.

Der Strom fließt dabei über den Widerstand Rshunt, wodurch dort eine in entsprechendem Verhältnis stehende Spannung entsteht. Diese Spannung wird zu den Ausgängen Output 1 und Output 2 geführt. Die Differenzspannung dieser beiden Ausgänge steht im direkten Verhältnis zum Strom am Eingang. 

Die Widerstände R1 und R2 dienen als Spannungsteiler, welcher den Shunt-Widerstand auf ein DC-Potential von 1.65V (= Hälfte der Versorgungsspannung) hebt. Dadurch können sowohl die positiven, als auch negativen Halbwellen (Strom) gemessen werden.

##### Dimensionierungshinweis
Der Shunt-Widerstand wurde auf 330 Ohm dimensioniert. Die Dimensionierung ist von der Versorgungsspannung (in unserem Fall 3.3V durch den µC), sowie den zu messenden Strombereich abhängig.

Für die Dimensionierung gilt: Es werden positive und negative Halbwellen gemessen, daher teilt sich der zur Verfügung stehende Spannungsbereich (3.3 V) durch zwei (1.65 V). Diese Spannung ergibt zusammen mit dem Shuntwiderstand den maximalen Shunt-Strom, welcher gemessen werden kann (I = U / R). In unserer Dimensionierung mit 330 Ohm ergeben sich 5mA, als Shunt-Strom. Da die Stromzange im Verhältnis 2000 : 1 übersetzt, kann somit ein maximaler Strom von 10 A gemessen werden.

**Achtung**
Die 10A beziehen sich auf den Spitzenwert des Stromes, der Effektivwert ist dabei nur 7.07A. Sollen größere Ströme gemessen werden, muss der Shunt-Widerstand geringer dimensioniert werden.

Ein größerer Widerstand reduziert den Messbereich, bietet jedoch den Vorteil, dass größere Spannungen entstehen, welche leichter weiterverarbeitet werden können.

#### Ermittlung des Stromes
##### Einzelne Messpunkte
Unabhängig vom Mikrocontroller wird am Ausgang die Differenzspannung mithilfe eines ADCs ermittelt.

Daraus kann relativ einfach auf den Strom, welcher im ursprünglichen Leiter fließt zurückgerechnet werden. Die nachfolgenden Rechnungen sind von folgenden Parametern abhängig:

```
m = 2000     //Übersetzungsverhältnis der Stromzange

Rshunt = 330 //Dimensioierung siehe oben

n = 12       //Auflösung des ADCs
Uref = 3.3   //Referenzspannung des ADCs

```

Aus dem Digitalwert "D" ergibt sich die Spannung am Shunt als:
```math
{U_{Shunt} = D * \frac{U_{ref}}{2^n}}
```
Daraus folgt der Strom durch den Shunt:
```math
I_{Shunt} = \frac{U_{Shunt}}{R_{Shunt}}
```

Daraus wiederum der Strom durch den ursprünglichen Leiter:
```math
I_{mess}=I_{Shunt}*m
```
Die Formeln lassen sich zu folgendem Zusammenhang zwischen digitalem Messwert und Strom zusammenfassen:
```math
I_{mess}=D*\frac{U_{ref}*m}{R_{Shunt}*2^n}
```
Zu erwähnen ist, dass es sich bei allem, mit ausnahme von D und dem Strom um konstanten handelt. Für die Umrechnung, können diese Werte also zu einem einzelnen, konstanten Faktor zusammengefasst werden.

##### Effektivwert
Oben dargestellt ist, wie einzelne Messwerte errechnet werden können. Jedoch handelt es sich tatsächlich, wie bereits erwähnt um Wechselstrom, daher schwankt der obige Wert andauernd. Um auf Leistung, o.Ä. zurückschließen zu können ist es daher nötig, den Effektivwert zu berechnen.

Der Effektivwert beschreibt jenen DC-Strom, bei dem die selbe Leistung übertragen wird, wie beim entsprechenden Wechselstrom.

Um den Effektivwert zu ermitteln stehen grundsätzlich zwei Möglichkeiten zur Verfügung:

1. Idealer Sinus
Trifft man die Annahme, dass der fließende Strom eine Sinus-Signalform aufweist, wie dies die Netzspannung tut, gilt für den Effektivwert des Signales der einfache Zusammenhang: 
```math
I_{Eff}=\frac{I_{Spitze}}{\sqrt(2)}
``` 
Weicht die Signalform jedoch von einem Sinus ab, stimmt diese Rechnung nicht mehr. Daher wird die zweite Möglichkeit zur Bestimmung angewendet.

2. True-RMS
Auf Multimetern liest man hierzu häufig "True-RMS", dabei wird nicht von einem Sinus ausgegangen, sondern der wirkliche Effektivwert errechnet bzw. messtechnisch bestimmt.
Für den Effektivwert gilt die Formel: 
```math
I_{eff}=\sqrt{\frac{\sum{I^2}}{n}}
```
Um diese Berechnung anzuwenden müssen jedoch genügent Messpunkte gesammelt werden, um ein genaues Ergebnis zu erzielen.

Genaueres zur angewendeten Berechnung folgt noch.

## LoRa
Der oben ermittelte Strom soll später mit Hilfe von LoRa übertragen werden. Wichtige Grundlagen sind unter [LoRa.md](LoRa.md) zusammengefasst.

Für unsere Programmierung kommt wie oben erwähnt das STM32WL55 zum Einsatz. Dieses wird mithilfe der STMCubeIDE programmiert (für mich sehr neu, ungewohnt, deutlich komplizierter als das was ich aus der Schule gewohnt bin). Als Vorlage wird das Beispielprojekt "LoRa End Node", welches aus nachfolgender Datei zu entnehnmen ist verwendet [How to build a lora application with STM32Cube](attachements/an5406-how-to-build-a-lora-application-with-stm32cubewl-stmicroelectronics.pdf).

Diese hat das Senden von Daten mit LoRa im Prinzip bereits implimentiert, muss also nur noch verstanden und angepasst werden.

### Kommunikationsparameter für die Verbindung
Für die LoRa Verbindung selbst wurden folgende Kommunikationsparameter eingestellt:

```
DeviceEUI = 00, 80, E1, 15, 00, 0A, 98, F4
Application Key = 2B,7E,15,16,28,AE,D2,A6,AB,F7,15,88,09,CF,4F,C3
Nework Key = 2B,7E,15,16,28,AE,D2,A6,AB,F7,15,88,09,CF,4F,3C
App/Join EUI = 21, 21, 21, 21, 21, 21, 21, 21
Nework Session Key = 2B,7E,15,16,28,AE,D2,A6,AB,F7,15,88,09,CF,4F,3C
Application Session Key = 2B,7E,15,16,28,AE,D2,A6,AB,F7,15,88,09,CF,4F,3C
```

Wichtig ist dabei die DeviceEUI, diese ist auf dem Board selbst als Sticker aufgebracht. Sie identifiziert das Gerät selbst.
Die restlichen Daten können im Prinzip beliebig gewählt werden, sie müssen jedoch mit der Applikation im The Things Network übereinstimmen.

Als End-Stelle wird auf dem The Things Network eine neue Applikation erstellt. Diese trägt in diesem Fall den Namen "Testapp". Dort wird ein neues Gerät hinzugefügt. Die Daten werden dort gleich wie oben eingestellt. 

**Hinweis**
Die wichtigsten Einstellungen welche beim Hinzufügen getroffen wurden sind:

```
Frequency plan = Europe863-870 MHz (SF9 for RX2 - recommended)
LoRaWAN version = 1.0.3
Regional Parameters version = RP001 Regional Parameters 1.0.3 revision A
```
Wichtig ist, dass auch im STM32-LoRa Programm die Version 1.0.3 ausgewählt werden muss. Standardmäßig ist diese auf 1.0.4 eingestellt, was jedoch zu Problemen geführt hat.

Im Live-Data-Tab können die Daten, welche vom Endgerät empfangen werden betrachtet werden. Für jedes Datenpaket gibt es einen "Payload", das ist jener Teil, der später Sensordaten, ... enthalten kann. Übertragen werden jedoch nur einzelne Bytes, die Daten müssen selbstständig codiert/entcodiert werden.

### Wichtigste Programmfunktionen
Das bereits exisiterende End Node Template, hat bereits alle Funktionen zum Senden von Daten bzw. Empfangen implimentiert. Der gesamte LoRa-Ablauf wird über die Datei "lora_app.c" gesteuert.
Die entscheidendsten Funktionen sind hierbei:

`SendTxData` - Diese Funktion wird, wenn eine Verbindung mit einem LoRa Gateway hergestellt wurde aufgerufen um Daten zu übertragen. Dazu steht ein Timer zur Verfügung, welcher alle X-Sekunden ein Sende-Event auslöst.
Im Sende Event wird der Payload übertragen, in diesen können die gewünschten Daten eingefügt werden. Angesprochen wird dieser im obigen Fall durch das Byte-Array AppData.Buffer. Der Payload besteht aus einzelnen Bytes.

`OnRxData` - Diese Funktion wird immer aufgerufen, wenn unser LoRa Gateway Daten empfängt. Empanfgen werden können die Daten dabei jeweils immer nachdem gesendet wurde.


## STM32 Datenerfassung
Die Datenübertragung erfolgt wie oben erwähnt alle X-Sekunden.

Übertragen werden dabei jeweils:

### RMS-Wert des Stromes
Um diesen zu ermitteln müssen Messpunkte gesammelt werden. Diese sollten im ms Bereich aufgezeichnet werden. Aus den Messpunkten, wird vor jeder Datenübertragung, nach der Rechnung von oben der ADC der Effektivwert des Stromes, welcher in dieser Zeit geflossen ist ermittelt. Verwendet werden dabei die ADC-Eingänge 1 und 3, welche PB14 und PB4 des Boards bzw. die Anschlüsse "A4" sowie "A3" darstellen.

Zum Messen im ms Abstand bietet sich die Verwendung eines Timers an. Das LoRa Programm, welches verwendet wird funktioniert jedoch in kombination mit den Timern, welche das STM32 zur Verfügung stellt nicht.

Das LoRa Programm selbst bietet jedoch auch interne Timer an. Nachteil ist, dass hier die minimale Periodendauer auf 3ms beschränkt ist. Für unser Programm wurde dies jedoch als ausreichend empfunden. Es wird also alle 3ms ein Messereignis ausgelöst.
In einem Messereignis werden mithilfe der Methoden `SYS_GetChanel3Value` und `SYS_GetChanel1Value`, welche programmiert wurden, die ADC-Werte eingelesen. Anhand dieser kann der Strom, welcher in diesem Moment geflossen ist ermittelt werden.

#### Bestimmung

Betrachtet man die Formel für den Effektivwert, welche verwendet wird:
```math
RMS = \sqrt{\frac{\sum{I²}}{n}}
```
So ist zu erkennen, dass es ausreicht bei jedem Timer-Event den Strom zu berechnen, das Quadrat daraus zu bilden und zur Summe der bisherigen Messwerte hinzuzuzählen.
Im Hintergrund läuft ein Zähler mit, welcher zählt, wie viele Messungen gemacht wurden.

Immer wenn ein "Upload-Event" ausgelöst wird, wird nun die fortlaufende Summe durch die Anzahl dividiert und die Wurzel gezogen. Man erhält den Effektivwert.
Die zwei Variablen werden auf 0 zurückgesetzt.

Um die Summe zu bilden wird eine double verwendet (8-Bytes). Hochgeladen wird letztendlich "nur" noch eine Float-Zahl (4-Bytes).

#### Upload Zeit
Das Hochladen von Daten sollte mit LoRa nur im Minuten Bereich erfolgen. Es ist zwar möglich die Zeit auch auf 10 Sekunden einzustellen, jedoch ist dies eigentlich nicht zu empfehlen. Standardmäßig wird die Upload-Zeit auf 120 Sekunden den festgelegt.
Jedoch wird alle 10 Sekunden überprüft, ob der aktuelle RMS-Messwert vom bisherigen abweicht (>50mA), wenn dies der Fall ist, wird ein Upload-Event ausgelöst. So ist es nicht nötig, extrem viele Daten zu senden, gleichzeitig wird bei einer Änderung schnell ein Datenwert gesendet.



### Temperatur & Luftfeuchtigkeit
Die Temperatur und Luftfeuchtigkeit werden mithilfe des angeschlossenen Temperatursensors DHT11 ermittelt. Der Sensor liefert serielle Daten, welche an PA1 empfangen werden.

Auch hier kommt eine Libary, welche etwas modifiziert wurde, zum Einsatz:
[How to use DHT11 sensor with STM32 » ControllersTech](https://controllerstech.com/using-dht11-sensor-with-stm32/)

### RTC
An das Board wurde per I2C-Schnittstelle eine RTC angeschlossen.
Zum Einsatz kommt das RTC-Modul DS1307-V03 [Datenblatt des RTC-Bausteins](attachements/datasheet.pdf).

Letzendlich kommt eine Library dafür zum Einsatz: https://github.com/eepj/DS1307_for_STM32_HAL/

Verwendet wird die I2C1 Schnittstelle des Boards. SDA ist dabei PA10, SCL PA9.

Die RTC kommt zum einsatz um die Dauer der Messung in Sekunden zu ermitteln und hochzuladen. Daher: Beim Starten der aktuellen Messung wird die Zeit gestoppt und dann, wenn ein Messwert hochgeladen wird.

## STM32 Ausgabe
Der STM32 dient nicht nur dazu Messwerte, etc. zu erfassen, sondern auch für die Ausgabe von Dingen. Dazu stehen folgende zwei Dinge zur Verfügung:
  - 3 LEDs auf dem Board
  - 4 stellige 7 Segment Anzeige

## Datenformatierung - Uplink
Um kurz nochmals festzuhalten. Ermittelt und hochgeladen werden sollen:
1. RMS-Strom welcher über X-Sekunden ermittelt wurde (float)
2. Anzahld der Messpunkte, welche dafür nötig waren (uint32)
3. Dauer des Ganzen in Sekunden (uint16)
4. Temperatur (uint8)
5. Luftfeuchtigkeit (uint8)

Dazu sendet unser Board alle X-Sekunden eine Uplink-Message, welche Bytes als Payload enthält.

---

==Payload-Bytes==
Byte 0  - Dauer über die der RMS ermittelt wurde in Sekunden - Byte 1
Byte 1  - Dauer über die der RMS ermittelt wurde in Sekunden - Byte 2
Byte 2  - Temperatur in °C
Byte 3  - Luftfeuchtigkeit in %
Byte 4  - RMSStrom - float Byte 4
Byte 5  - RMSStrom - float Byte 3
Byte 6  - RMSStrom - float Byte 2
Byte 7  - RMSStrom - float Byte 1
Byte 8  - Anzahl der Messpunkte - uint32 Byte 1
Byte 9  - Anzahl der Messpunkte - uint32 Byte 2
Byte 10 - Anzahl der Messpunkte - uint32 Byte 3
Byte 11 - Anzahl der Messpunkte - uint32 Byte 4

---

Byte 3 - 6 bilden die Float, welche den Effektivwert des Stromes darstellt. Die Float besteht aus 4-Bytes, welche einzeln hochgeladen werden. Byte 4 ist dabei die obersten 8 Bit, ...
![](attachements/Pasted_image_20220728165110.png)

Byte 0 & 1 bzw. Byte 8/9/10/11 bilden jeweils unsigned Integers, wobei das erste Byte jeweils die niederwertigeren Bytes darstellt.

Konkret sieht das ganze im C-Code wie folgt aus:

```C
float rmsValue = sqrt(SumOfSquaredCurrents/ADCMeasurementCounter); //Calculates the RMS-Current-Vale

uint16_t endSecond = DS1307_GetSecond();
unsigned char endMinute = DS1307_GetMinute();
unsigned char endHour = DS1307_GetHour();

if(endMinute < StartMinute)
{
 endMinute += 60;
}

endSecond += (endMinute-StartMinute)*60;
endSecond -= StartSecond;

uint8_t uintAsbytes[2];
memcpy(uintAsbytes, &endSecond, sizeof(endSecond));

AppData.Buffer[i++] = uintAsbytes[0];
AppData.Buffer[i++] = uintAsbytes[1];

UTIL_TIMER_Stop(&ADCMeasTimer);

DHT_GetData(&DHT11_Data);

UTIL_TIMER_Start(&ADCMeasTimer);
AppData.Buffer[i++] = (uint8_t)DHT11_Data.Temperature;
AppData.Buffer[i++] = (uint8_t)DHT11_Data.Humidity;

if(rmsValue > 0.02)
{
 rmsValue = 0;
}
 unsigned char floatBytes[4] = {0};

 union {
  float a;
  unsigned char bytes[4];
 } thing;
thing.a = rmsValue;
memcpy(floatBytes, thing.bytes, 4);

AppData.Buffer[i++] = floatBytes[3];
AppData.Buffer[i++] = floatBytes[2];
AppData.Buffer[i++] = floatBytes[1];
AppData.Buffer[i++] = floatBytes[0];

uint8_t bytes[4];
memcpy(bytes, &ADCMeasurementCounter, sizeof(ADCMeasurementCounter));
		  
AppData.Buffer[i++] = bytes[0];
AppData.Buffer[i++] = bytes[1];
AppData.Buffer[i++] = bytes[2];
AppData.Buffer[i++] = bytes[3];
```
Strom Werte unter 20mA werden dabei verworfen, da sich die Stormzange mit einem Übersetzungsverhältnis von 1:2000 für deren Messung nicht mehr eignet und so floats wie 1.23e-32, etc. verhindert werden können.

In der Applikation auf TheThingsStack erhalten wird als Payload nun z.B. folgende Daten:

`0x0A 0x00 0x1C 0x2A 0x00 0x00 0x00 0x00 0x56 0x0D 0x00 0x00`

Es handelt sich um unsere 12-Byte in hex-Darstellung.

Wir können im Web nun einen Uplink-Payload-Formtter angeben, welcher aus diesen Daten wieder die ursprünglichen Werte errechnet. 

(Uplink-Payload-Formatter)[UplinkPayloadFormatter.js]

In LiveData erhalten wir so den Output: 
![](attachements/Pasted_image_20220728165537.png)

Unsere Daten wurden hochgeladen und dekodiert. Konkret können diese nun als JSON ausgegeben/bezogen werden. Das Datenpaket besteht aus viel mehr Dingen (Adresse, Verbindungsdetails, ...). Aber eben auch aus dem dekodierten Payload:

```JSON
...
decoded_payload": {
        "measDurration": 10,
        "measHumidity": 43,
        "measPointsForRMS": 3412,
        "measTemp": 28,
        "rmsCurrent": 0
      },
...
```

## Datenformatierung - Downlink
Um einzustellen, was ausgegeben wird werden Downlinks verwendet. Die Uplinks sind die Daten, welche das LoRa Endgerät an das Gateway sendet. Die Downlinks sind die Daten, welche das LoRa Endgerät vom Gateway empfängt.

Die Downlinks dienen dazu, die Ausgabe auf den LEDs zu steuern, die Ausgabe auf der 7-Segment-Anzeige zu steuern, sowie Programmeinstellungen zu ändern.

**Hinweis**
Bei den Downlinks ist zu beachten, dass pro Uplink, nur 1 Downlink gesendet werden kann. Das heißt auch, dass wenn die Uplink-Zeit auf einen großen Wert gestellt wird, die Downlinks auch so lange benötigen. 

---

==Port 5==
Mit diesem Port werden die 3 LEDs auf dem Board angesprochen. Dazu wird nur 1 Byte übertragen.

Bit 0-2 enthalten dabei die Information über die 3 LEDs (ein = 1, aus = 0)
Ist Bit 3 gesetzt, werden alle LEDs ausgeschaltet.
Ist Bit 4 gesetzt, werden alle LEDs eingeschaltet, wobei ausschalten > einschalten geweretet ist 

==Port 6==
Mit diesem Port wird das 4-Ziffern 7-Segment Display angesprochen. Dazu werden insgesamt 5 Bytes übertragen.

Byte 1 enthält dabei den "Modus":
  - 0: Das Display wird ausgeschaltet
  - 1: Das Display zeigt eine Zahl an
  - 2: Das Display zeigt die Uhrzeit der RTC an
  - 3: Das Display zeigt den zuletzt hochgeladenen Strom an 

Byte 2/3/4/5 enthalten dabei die Ziffern, welche in Modus 1 angezeigt werden sollen.
Es ist möglich die Ziffern 0-15 anzuzeigten (0-A), sowie ein Minus Symbol "-", welches über die Zahl 16 erzeugt wird. Alles andere (>16) führt dazu, dass das Segment ausgeschaltet wird.

**Hinweis**
Das Rechte obere Segement von Ziffer 1 funktioniert nicht.

==Port 7== 
Mit diesem Port kann die Zeit, nach welcher jeweils Daten hochgeladen werden, verändert werden. Hierzu wird hier die gewünschte Periodendauer in Sekunden angegeben. Übertragen werden dazu 2 Bytes (uint16, wobei das erste Byte den niederwertigeren Teil bildet)

**Hinweis**
Die Zeit zum Hochladen kann minimal 10 Sekunden annehmen (bzw. auch geringere Werte, die Uploads geschehen dennoch nur alle 10 Sekunden).

---

Auch für die Downlinks ist es nötig, einen Formatter einzustellen. Dieser dient dazu, die ihm übergebenen Daten (JSON) in Bytes umzurechnen, welche an das Board übertragen werden sollen.

(Downlink-Payload-Formatter)[DownlinkPayloadFormatter.js]

## Datenverarbeitung mit TheThingsNetwork
Oben wurde angesprochen, welche Uplink Daten wir erhalten (JSON mit "Decoded Payload").
Daten bringen nur dann etwas, wenn sie auch weiterverarbeitet werden.

Lädt man die Daten einfach nur hoch, werden einem diese in "LiveData" angezeigt. Macht man das Browser Fenster zu, werden die Daten jedoch gelöscht.

Es gibt jedoch die Möglichkeit sich über Webhooks, MQTT, ... die Daten zu besorgen/zu verarbeiten. Das The Things Network ist dabei recht flexibel.

Angewendet wurden in meinem Fall zwei Dinge:
  - Storage Integration
  - Webhook

### Storage Integration
Storage Integration muss einfach aktiviert werden. Dann werden alle Daten, die man ansonsten in Live data angezeigt bekommen würde, hier in einer Datenbank abgespeichert. Auf die Datenbank kann später per API zugegriffen werden.

**Nachteile**
- Die Daten werden in der kostenlosen Version des The Things Network nur für eine begrenzte Zeit abgespeichert. (30 Tage, 1 Woche oder 24 Stunden, tatsächlich bin ich mir nicht sicher?).

- Die Storage Integration wird von der Website jedoch nicht für das Herunterladen von aktuellen Daten (sprich: polling) empfohlen, sondern um sich alte Daten zu holen, da die Daten vom Empfangen bis zum abspeichern in der Datenbank eine gewisse Zeit brauchen können bzw. geblockt werden.


### Webhook
Es lässt sich zustätzlich oder alternativ eine Webhook einrichten, an die die Empfangenen Daten weitergeleitet werden. Es gibt kostenlose Webhooks Hosts im Internet (z.B. https://webhook.site/), die bis zu 500 Datenpakete annehmen, auf welche ebenfalls wiederum per API zugegriffen ewrden kann. Die Webhooks eigenen sich dazu, aktuelle Daten zu erhalten, da die Daten beim Empfang direkt an diese weitergeleitet werden.


### JSON Daten
Beide oben erwähnten Dinge wurde eingerichtet und wird nachfolgend auch verwendet.

Die Daten sehen dabei jeweils wie in etwa wie folgt aus (bei der Webhook gibt es davor noch einen Header) und können über die jeweiligen APIs bezogen werden.

Es handelt sich um JSON-Daten, wobei jede Uplink-Nachricht so abgespeichert wird. Besonders interessant ist für uns eigentlich nur die Uhrzeit, wann die Daten gepushed wurden (revieced_at) (Achtung: 2h hinter unserer Zeit), sowie der "decoded_payload", welcher unsere Daten enthält. Der Rest ist im Prinzip hauptsächlich Informationen über die Verbindung und für uns nicht wirklich interessant.

[Beispiel Datenpaket](ExampleDataPackage.json)

## Desktop Applikation
Es gitb also zwei Datenquellen, auf welche per API zugegegriffen werdne kann. Dort werden alle Daten, welche hochgeladen werden gesammelt.
Um diese Daten sammeln und darstellen zu können wurde eine Desktop Applikation (WPF, C#) realisiert.

### Aufbau der Applikation
Die JSON-Daten, welche von den APIs geholt werden, werden auf die relevanten Payload-Daten, sowie die Uhrzeit gefiltert/herunter gekürzt.
Intern wird das Ganze dann in einer Klasse "CurrentMeasurementPoint" gespeichert. Für jeden Datenpunkt wird ein solches Objekt erstellt. Darin wird Uhrzeit der Messung, Dauer, Luftfeuchtigkeit, Temperatur, RMS-Strom und noch Daten für die Applikation selbst, abgespeichert. Außerdem gibt es eine Klasse "CurrentMeasurementsList", welche das Verwalten aller Datenpunkte (Speichern/Öffnen/...) ermöglicht.

#### Klassendiagramme

|![](attachements/Pasted_image_20220729085447.png)| ![](attachements/Pasted_image_20220729085442.png)|
     
#### Aufbau des UIs
Das UI besteht aus 3-Tabs

##### Datagrid
Dieser Tab enthält, wie der Name sagt einfach ein Datengrid (Tabelle), in welcher alle gesammelten Daten dargestellt werden. Einzelne Datenpunkte können auch gelöscht werden.

![](attachements/Pasted_image_20220803154204.png)


##### Plot
Dieser Tab ermöglicht es die geladenen Messpunkte als Graph darstellen zu lassen.

![](attachements/Pasted_image_20220803154218.png)

Links oben kann der anzuzeigende Zeitraum ausgewählt werden.

Über Checkboxen können einzelne Messgrößen ein-/ausgeschaltet werden (Strom, Temperatur, Luftfeuchtigkeit).

Mit einem Slider kann eingestellt werden, dass jeweils 1-50 Datenpunkte zu einem Plot-Punkt zusammengefasst werden (durchschnitt) -> dadurch werden nicht unnötig viele Datenpunkte geplottet, was vor allem bei längeren Zeitpunkten die darstellung verbessert.

Der dargestellte Graph verfügt für jede Messgröße eine Achse (Strom links, Temperatur/Luftfeuchtigkeit rechts). Genaue Messwerte eines Messpunktes können durch Hovern der Maus darüber angezeigt werden.

##### Downlink and Save
Dieses Fenster ermöglicht es Nachrichten an das Board zu senden bzw. die Messpunkte, welche geladen sind in einer Datei abzuspeichern/den Speicherort welcher verwendet wird zu ändern.

![](attachements/Pasted_image_20220803154239.png)

Außerdem ist es möglich eine völlig neue Datei, ohne alte Daten zu erstellen oder eine gespeicherte Datei zu laden. Auch können die Daten als CSV abgespeichert werden.

Außerdem kann die Applikation durch "Run in Background" minimiert werden, sie läuft dann nur noch im Hintergrund und wird im System Tray angezeigt. Durch Doppelklick kann sie wieder angezeigt werden. So werden weiterhin Daten heruntergeladen und abgespeichert, ohne die richtige Applikation ausführen zu müssen. Resourcenintensiv an der Applikation selbst ist grundsätzlich nur das Plotting, welches dadurch nicht mehr nötig ist. Die CPU wird quasi nicht mehr belastet. Nur der Arbeitsspeicher muss noch die ganzen Measurement Points halten, was sich jedoch grundsätzlich auch in einem gewissen Rahmen halten sollten.

Die Downlink Nachrichten ermöglich das Senden aller oben beschriebenen Funktionen.
Es ist möglich, die drei auf dem Board verbauten LEDs zu steuern, einzustellen, was auf dem eingebauten 7-Segment Display angezeigt wird (Gewünschte Zahl, Uhrzeit, Gemessener Strom, Aus). Außerdem kann die Periodendauer, mit welcher das Board Daten hochlädt eingestellt werden.
Dies geschieht durch senden einer Downlink-Nachricht an das Board. Die Applikation kann dies durch Zugriff auf eine API tun.
Das Board wird beim Empfangen von Daten die Funktion `OnRxData` aufgerufen. Dort werden die Empfangenen Daten verarbeitet.

### Sammeln der Messdaten
#### Prinzip
Beim 1. Starten der Applikation ein Speicherort ausgewählt (kann geändert werden). An diesem wird vom Programm eine SQLite-Datenbank erstellt (im Prinzip einfach nur eine lokale Datei). Dort werden die Daten, welche von der Applikation gesammelt werden längerfristig offline abgespeichert.

So löst sich einerseits das Problem, dass das TheThingsNetwork die Daten nur begrenzt speichert (sofern die Applikation regelmäßig ausgeführt wird) und der Zugriff auf das Web ist nur für "neue Daten" nötig, nichtmehr für Daten, welche bereits heruntergeladen wurden.

Insgesamt haben wir für die Applikation also 3 Datenquellen. 
- Die Datenbank, welche lokal gespeichert wurde und sämtliche Daten enthält, die bereits in vorherigen Sessions heruntergeladen wurden.
- Die Storage Integration Cloud, welche langfristig die empfangenen Daten speichert
- Der Webhook, welcher es ermöglicht aktuelle Daten zu beziehen

Der Ablauf, mit welchem unser Programm ab dem Starten Daten sammelt ist dabei so:

<img src="attachements/Pasted_image_20220729094554.png" height="1000">

1. Es werden lokale Daten geladen
2. Alle in der Storage Integration gespeicherten, neuen Daten werden bezogen
3. Alle in der Webhook gespeicherten, neuen Daten werden bezogen und lokal gespeichert
4. Alle Webhook Daten werden gelöscht (weil dort nur 500 Pakete gespeichert werden können, und die alten Daten onehin in der Storage Integration liegen)
5. Alle X-Sekunden wird die Webhook auf neue Daten überprüft und diese lokal abgespeichert

...X-Sekunden = 10 beim starten, minimiert, geschieht dies in einer 10 mal so langen Periodendauer. Wenn die Uplink-Periode des Boards durch eine Downlink-Nachricht verändert wird geschieht das herunterladen nur noch in dieser Periodendauer

So werden stets die aktuellen Daten des Sensors bezogen, ohne, dass es durch die verschiedenen Datenquellen zu Duplikaten kommt. Alle neuen Daten werden fortlaufend in der lokalen Datenbank abgespeichert.

![](attachements/Pasted_image_20220729094843.png)

Während der Laufzeit des Programmes werden alle Datenpunkte, welche zur Verfügung stehen in einer Liste der obigen Klasse CurrentMeasurementsList.cs gehalten.

#### Storage Integration API Zugriff
Das C#-Snippet für den Zugriff, welcher alle Daten nach einem gewünschten Datum liefert sieht wie folgt aus:
```C#
string key = "..."; //API Key

CurrentMeasurementPoint latestExisitingPoint = GetLatestMeasurement();
DateTime utcTime = latestExisitingPoint.Stop.ToUniversalTime();

HttpResponse<string> response = Unirest.get($"https://eu1.cloud.thethings.network/api/v3/as/applications/testapp12349182/packages/storage/uplink_message?after={utcTime:yyyy-MM-dd}T{utcTime:HH:mm:ss}Z")
.header("Authorization", $"Bearer {key}")
.asJson<string>();

string web = response.Body.ToString();
```
"web" enthält nun die JSON-Daten, aller Uplink-Messages, welche nach dem gewünschten Timestamp hochgeladen wurden.
Die Zeit wird auf UTC umgerechnet, da die Datenbank darauf arbeitet, lokal jedoch mit der lokalen Zeit gearbeitet wird.

Aus diesen JSON-Daten können durch String-Operationen (Trimmen, Teilen, ...) die Daten für die neuen Messpunkte ermittelt werden. Diese werden der lokalen Liste hinzugefügt und in die lokale Datenbank gespeichert.

#### Webhook API Zugriff
Dieser sieht im Prinzip genau gleich aus wie der obige Zugriff. Es wird jedoch nicht mit einem key, sondern einem Token gearbeitet
```C#
string token = "..."; //Token for Application
 HttpResponse<string> response = Unirest.get($"https://webhook.site/token/{token}/requests?sorting=newest")
.header("Accept", "application/json")
.asJson<string>();

string web = response.Body.ToString();
```
"web" enthält auch hier wieder die bezogenen Daten. DIese werden auch hier wieder, sofern sie noch nicht lokal vorhanden sind, der Liste hinzugefügt und in die Datenbank abgespeichert.

Da die Webhook-Website nur 500 Datensätze speichert (free-version), werden immer nach beziehen der letzten Datensätze diese gelöscht (werden auch nicht mehr benötigt, da sie ab nun in der lokalen Datenbank liegen).

Um alle bisherigen Daten zu löschen dient folgender Zugriff:
```C#
string token = "..."; //Application Token
Unirest.delete($"https://webhook.site/token/{token}/request")
.header("Accept", "application/json")
.asJson<string>();
```

### Downlink Nachrichten per API senden
Wie oben bereits dargestellt, wird mit der Applikation auch das Board gesteuert. Daz umüssen Downlink-Nachrichten über die API erzeugt werden. Der Body enthält dabei, jene Inhalte, welche mit dem Downlink-Payload-Formatter übersetzt werden um die Bytes zu erzeugen, welche an das Board gesendet werden.

fPort ist dabei der Port welcher angesprochen wird, laut dem festgelegten Datenformat für Downlinks ist dieser 5 für LEDs, 6 für die 7-Segment Anzeige und 7 für das Ändern der Periodendauer.

Nachfolgend ist beispielhaft der Zugriff auf die LEDs dargestellt, wobei dieser im Prinzip für die anderen Dinge gleich aufgebaut ist. Man übergibt im Body die Downlinks, welche im JSON-Format sind und die gleichen Bezeichnungen wie der Downlink-Payload-Formatter verwenden müssen.

```C#
string key = "..."; //API-Key

StringBuilder sb = new StringBuilder();
sb.Append("{\"downlinks\":[{\"decoded_payload\":{");
sb.Append($"\"led1\":\"{cbLED1.IsChecked}\",");
sb.Append($"\"led2\":\"{cbLED2.IsChecked}\",");
sb.Append($"\"led3\":\"{cbLED3.IsChecked}\",");

bool allOn = false;
bool allOff = false;
if(sender as Button != null)
{
  Button tmp = sender as Button;
  if(tmp.Tag.ToString() == "alloff")
  {
    allOff = true;
  }else if(tmp.Tag.ToString() == "allon")
  {
     allOn = true;
  }
}

sb.Append($"\"allOff\":\"{allOff}\",");
sb.Append($"\"allOn\":\"{allOn}\"");
sb.Append("},\"f_port\":5");
sb.Append("}]}");

Unirest.post($"https://eu1.cloud.thethings.network/api/v3/as/applications/testapp12349182/devices/eui-0080e115000a98f4/down/replace")
.header("Content-Type:", "application/json")
.header("Authorization", $"Bearer {key}")
.body(sb.ToString())
.asJson<string>();
```

## Aufbau des Boards
Der gesamte Aufbau wurde auf eine Lochraster Platine gelötet. Diese ist nachfolgend zu sehen. Alle größeren Dinge (Stromzange, RTC, Umwelt, Display, STM32WL55) werden einfach aufgesteckt. 

Jeder Sensor kann theoretisch auch weggelassen werden, dann werden die Messwerte schlicht mit 0 ersetzt bzw. ergeben sich als solches. Gewisse Programmfunktionen benötigen jedoch die Messwerte um richtig zu funktionieren (RTC für Abschätzen der Leistung, Temperatur/Luftfeuchtigkeit um diese zu plotten, Display um Ausgaben machen zu können)

**Header ohne Bauteile**
![](attachements/Pasted_image_20220802133649.png)


**Header mit Bauteilen**
![](attachements/Pasted_image_20220802133157.png)


## Tests & Beobachtungen
### 1. Genauigkeit der Strommessung
Hierzu wurde zusätzlich zur Strommessung zusätzlich zu unserer Stromzange ein Multimeter (Agilent U1252B) verwendet und das Kabel mit verschiedenen Stromflüssen belastet, die gerade zur Verfügung standen.

| Digital MM | Stromzange | Last           |
| ---------- | ---------- | -------------- |
| 0 A        | 0 A        | Keine          |
| 0.09 A     | 0.09 A     | Lötkolben idle |
| 0.13 A     | 0.13 A     | Lampe          |
| 0.5 A      | 0.5 A      | Bildschirm     |

Der Test ist nicht ausführlich. Jedoch liefert die Stromzange weitgehend den gleichen Ausgabewert wie das angeschlossene Multimeter. Es ist darauf zu achten, dass die Messung nur dann genaue Messwerte liefert, wenn die Stromzange richtig geschlossen wird.

### 2. LoRa Verbindungsreichweite
Theoretisch sollte LoRa eine sehr große Reichweite bieten (km-Bereich). Doch wie gut funktioniert die Verbindung?
Der Test ist recht simpel. Der Praktikant :) nimmt seinen Aufbau mit in den Zug, wo dieser per USB mit Strom versorgt wird. Auf dem Notebook kann man sich die Daten anschauen und nachvollziehen, bis wo/ab wo/... eine Verbindung bestanden hat.

### 3. Temperatur über den Tag
!()[/attachements/Pasted_image_20220804151133.png]

Man sieht einen relativ schönen Temperaturverlauf im Büro, über den Tag verteilt. Vorallem die Klimaanlage ist interessant zu beobachten.

## Probleme/Verbesserungen
### Downlink Verzögerung
Das wahrscheinlich noch größte Problem ist, dass Downlinks immer nur dann gesendet werden, wenn Uplinks erfolgen.
Daher: Wenn die Periode z.B. 1h beträgt, wird der gewünschte Downlink auch erst nach 1h übertragen. 

LoRa bietet hier eigentlich eine Lösung an: Sie unterschieden Class A, Class B und Class C Devices.

Class A Devices, empfangen Daten nur nach dem gerade Daten gesendet werden. Ansonsten ist kein Empfangsfenster offen.
Class B Devices empfangen Daten in bestimmten Rx Fenstern, welche mit einer gewissen Periode geöffnet werden.
Class C Devices empfangen immer Daten, außer wenn sie gerade senden.

Die Idee dahinter ist, dass durch das Offenhalten des Empfangsfensters deutlich mehr Strom verbraucht wird. Class A kann daher z.B. ein Sensor sein, der eigentlich keine Downlinks erhalten muss. Class B kann ein Gerät sein, welches nur selten Downlinks empfänge und Class C ein Gerät, welches immer Daten senden und empfangen soll.

Die Einstellungen wurden zwar auf ein Class C bzw. B Device angepasst, jedoch konnten die Daten weiterhin nur nach dem senden übertragen werden.


Aktueller Stand ist, dass wenn das Program als "Class A" definiert wird, die Downlinks in LoRa Version 1.0.3 immer nur bei einem Upload funktionieren, das Joinen jedoch sofort funktioniert.
In Version 1.0.4 als "Class C" funktioniren die Downlinks sofort, das Joinen jedoch nicht.

### 7 Segment Anzeige
Die Anzeige funktioniert, hat jedoch das Problem, dass ein Segment kaputt ist und die Segmente an der gleichen Stelle von anderen Zahlen dunkler leuchten als die restlichen Segmente. Hier ist die Hardware beschädigt worden, die Anzeige müsste jedoch nur ausgetauscht werden.

Das zweite Problem ist, dass es immer dann, wenn Daten hochgeladen werden zu einem kurzen Flackern kommt, da in dieser Zeit das Display nicht weiter aktualisiert werden kann. Eine Möglichkeit wäre, dass Display auf ein I2C Display o.Ä. zu ändern, da aktuell ständig zwischen den Zahlen gewechselt werden muss um ein scheinbar statisches Bild zu erzeugen.

### RTC
Die RTC funktioniert nur, wenn das Debuggen enabled ist.
