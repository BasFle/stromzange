#include <SoftwareSerial.h>

//Ref: 5V
//Bit: 10
//Rshunt : 47 R
//Stromübersetzung: 100A / 50mA 
// ->  0,0048828125 V / Bit
// -> 

#define Print

#include <TinyGPS++.h>//include the library code

#define Vref 5
#define Rshunt 330

TinyGPSPlus gps;
SoftwareSerial ss(4,3); //Serial connecting to the gps device

int analogVal = 0;

const float voltageConversionFactor = Vref/1024;
const float shuntCurrentConversionFactor = Vref/(1024*Rshunt);
const float actualCurrentConversionFactor = (Vref*100)/(1024*Rshunt*0.05);

void setup() {
  Serial.begin(9600);
  ss.begin(9600); //gps-module communication
  
  Serial.println("Serial ready.");
  
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);  

  analogReference(EXTERNAL);

  if (ss.available() > 0) {
    gps.encode(ss.read());
  }
}

void loop() {
   analogVal = analogRead(A0) - analogRead(A1);
#ifdef Print

 if (ss.available() > 0){
      Serial.print("Measurment at: ");
      Serial.print(gps.location.lat());
      Serial.print(" ");
      Serial.print(gps.location.lng());
      Serial.print(" Time:");
      Serial.print(gps.time.value());
 }
    
  Serial.print(" Digital Value: ");
   Serial.print(analogVal);

   Serial.print(" , Spannung an Shunt: [mV]: ");
   Serial.print(analogVal * voltageConversionFactor);

   Serial.print(" , Strom durch Shunt: [mA]: ");
   Serial.print(analogVal * shuntCurrentConversionFactor);

   Serial.print(" , Entsprechender Strom: [A]: ");
   Serial.println(analogVal*actualCurrentConversionFactor);

      #else
  Serial.println(analogVal);
  //Serial.println(analogVal*shuntCurrentConversionFactor);
#endif
}
