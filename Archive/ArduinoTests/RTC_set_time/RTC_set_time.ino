#include <DS1307.h> //RTC

DS1307 rtc;

  // put your setup code here, to run once:
void setup()
{
    Serial.begin(9600);
    rtc.begin();
    rtc.fillByYMD(2022,7,19);
    rtc.fillByHMS(14,27,45);
    rtc.fillDayOfWeek(TUE);//Saturday
    rtc.setTime();//write time to the RTC chip
}


void loop() {
  // put your main code here, to run repeatedly:

}
