#include <DS1307.h>

//Ref: 5V
//Bit: 10
//Rshunt : 47 R
//Stromübersetzung: 100A / 50mA 
// ->  0,0048828125 V / Bit
// -> 

#define Vref 5
#define Rshunt 328.70
#define windungen 1
#define measurementsPerOutput 10

#define RmsArrayLength 100
#define AverageRmsArrayLength 55  //rmsLength muss groß genug sein, ansonsten wird öfters als "secondsPerPush" gepushed.

#define secondsPerPush 10

int startUpSeconds = 0;

int analogVal = 0;
uint64_t prevMillis = 0;

const float voltageConversionFactor = Vref*1000.0 / 1024.0;
const float shuntCurrentConversionFactor = voltageConversionFactor/Rshunt;
const float actualCurrentConversionFactor = shuntCurrentConversionFactor*100.0/50.0;
const float actualCurrentConversionFactorSquared = actualCurrentConversionFactor*actualCurrentConversionFactor;

float curValues[RmsArrayLength]; 

float curSum = 0;

float pushSum = 0;

DS1307 rtc;

void setup() {
  Serial.begin(2000000);
  Serial.println();
  
  //Serial.println(actualCurrentConversionFactor,4);
  //Serial.println(actualCurrentConversionFactorSquared,4);
  //delay(2000);
  //Serial.println("Serial ready.");
  
  analogReference(EXTERNAL); 

  Serial.println("");

  rtc.begin();
  rtc.getTime();

  startUpSeconds = rtc.second;
  Serial.print("Start at: ");
  Serial.print(rtc.hour,DEC);
  Serial.print(":");
  Serial.print(rtc.minute,DEC);
  Serial.print(":");
  Serial.println(rtc.second,DEC);

  Serial.print("Pushing every: ");
  Serial.print(secondsPerPush);
  Serial.println(" seconds");
}

void loop() 
{
 //Serial.println(analogVal*actualCurrentConversionFactor);
 //Serial.println(analogVal*shuntCurrentConversionFactor);

 for(int a = 0; a < AverageRmsArrayLength; a++){
    for(int i = 0; i<RmsArrayLength; i++)
    {
       curSum -= curValues[i];
       
       analogVal = (analogRead(A0) - analogRead(A1));
       
       if(analogVal >= -1 && analogVal <= 1)
       {
          analogVal = 0;  //Removes Measurements which are -1/1, often occur when turned off.
       }
       
       curValues[i] = analogVal*analogVal*actualCurrentConversionFactorSquared/windungen;
     
       curSum += curValues[i];
    
       //if(i%measurementsPerOutput == 0){
         //Serial.print("Current True-RMS: ");
       //Serial.println(sqrt(curSum*1.00/RmsArrayLength),3);
       //}
    
       while((millis() - prevMillis) < 2){ }
    
       prevMillis = millis();  
     }
     
     pushSum += sqrt(curSum*1.00/RmsArrayLength);
  
     rtc.getTime();
  
     if(((rtc.second - startUpSeconds) % secondsPerPush == 0 && a > 10) || (a == AverageRmsArrayLength-1))  //no pushing under 10 Values for the Average = ~2s minimal push interval
     {   
       Push(a);  

       if(a == AverageRmsArrayLength-1){
        startUpSeconds = rtc.second;
       }
       break;
     }
   }
}

void Push(int a)
{
    Serial.print(rtc.hour, DEC);
    Serial.print(":");
    Serial.print(rtc.minute, DEC);
    Serial.print(":");
    Serial.print(rtc.second, DEC);
    Serial.print("  ");
    Serial.print("Last Period Average: ");
    Serial.print(pushSum/(a+1),3);
    Serial.print(" A or ");
    Serial.print(pushSum*230/(a+1),3);
    Serial.print(" W , based on:");
    Serial.print(a+1);
    Serial.println(" values");
  
    pushSum = 0;
}
