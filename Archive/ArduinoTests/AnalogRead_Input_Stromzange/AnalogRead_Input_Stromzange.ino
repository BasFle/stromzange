//Ref: 5V
//Bit: 10
//Rshunt : 47 R
//Stromübersetzung: 100A / 50mA 
// ->  0,0048828125 V / Bit
// -> 

#define Plot

#define Vref 5
#define Rshunt 328.70
#define rmsArrayLength 100
#define windungen 1
#define measurementsPerOutput 10

int analogVal = 0;
uint64_t prevMillis = 0;

const float voltageConversionFactor = Vref*1000.0 / 1024.0;
const float shuntCurrentConversionFactor = voltageConversionFactor/Rshunt;
const float actualCurrentConversionFactor = shuntCurrentConversionFactor*100.0/50.0;
const float actualCurrentConversionFactorSquared = actualCurrentConversionFactor*actualCurrentConversionFactor;

float curValues[rmsArrayLength]; 

float curSum = 0;

void setup() {
  Serial.begin(2000000);
  Serial.println();
  
  //Serial.println(actualCurrentConversionFactor,4);
  //Serial.println(actualCurrentConversionFactorSquared,4);
  //delay(2000);
  //Serial.println("Serial ready.");
  
  analogReference(EXTERNAL); 

  Serial.println("");
  #ifdef Print
  Serial.println("D\tmV\tmA\tA\tms");
  #endif
}

void loop() 
{
#ifdef Print
  analogVal = analogRead(A0)-analogRead(A1);

  // Serial.print("Digital Value: ");
  Serial.print(analogVal);
  Serial.print("\t");
  
  // Serial.print(" , Spannung an Shunt: [mV]: ");
  Serial.print(analogVal * voltageConversionFactor);
  Serial.print("\t");
  
  // Serial.print(" , Strom durch Shunt: [mA]: ");
  Serial.print(analogVal * shuntCurrentConversionFactor);
  Serial.print("\t");
  
  // Serial.print(" , Entsprechender Strom: [A]: ");
  Serial.print(analogVal*actualCurrentConversionFactor);
  Serial.print("\t");
   Serial.println();
  //Serial.println(millis() - prevMillis);
  prevMillis = millis();
#endif

#ifdef Plot
 //Serial.println(analogVal*actualCurrentConversionFactor);
 //Serial.println(analogVal*shuntCurrentConversionFactor);
  for(int i = 0; i<rmsArrayLength; i++)
  {
   curSum -= curValues[i];
   
   analogVal = (analogRead(A0) - analogRead(A1));
   
   if(analogVal >= -1 && analogVal <= 1)
   {
      analogVal = 0;  //Removes Measurements which are -1/1, often occur when turned off.
   }
   
   curValues[i] = analogVal*analogVal*actualCurrentConversionFactorSquared/windungen;
 
   curSum += curValues[i];

   if(i%measurementsPerOutput == 0){
     //Serial.print("Current True-RMS: ");
   Serial.println(sqrt(curSum*1.00/rmsArrayLength),3);
   }

   while((millis() - prevMillis) < 2){ }

   prevMillis = millis();  
   }
   
     /*
    int maxVal = 0;
    
    for(int i = 0; i<rmsArrayLength; i++)
    {
      if(analogValues[i] > maxVal)
      {
        maxVal = analogValues[i];
      }
    }
    Serial.print("By max:");
    Serial.println(maxVal*actualCurrentConversionFactor/sqrt(2));
*/
#endif
}
