#include <DS1307.h> //RTC

DS1307 rtc;

  // put your setup code here, to run once:
void setup()
{
    Serial.begin(2000000);
    rtc.begin();
}


void loop() {
  // put your main code here, to run repeatedly:
  rtc.getTime();
    Serial.print(rtc.hour, DEC);
    Serial.print(":");
    Serial.print(rtc.minute, DEC);
    Serial.print(":");
    Serial.print(rtc.second, DEC);
    Serial.print("  ");
    Serial.print(rtc.month, DEC);
    Serial.print("/");
    Serial.print(rtc.dayOfMonth, DEC);
    Serial.print("/");
    Serial.print(rtc.year+2000, DEC);
    Serial.print(" ");
    Serial.print(rtc.dayOfMonth);
    Serial.println("*");;
}
