//Ref: 5V
//Bit: 10
//Rshunt : 47 R
//Stromübersetzung: 100A / 50mA 
// ->  0,0048828125 V / Bit
// -> 
#define voltageConversionFactor 4.8828125        //mV
#define shuntCurrentConversionFactor 0.0147964   //mA
#define actualCurrentConversionFactor 0.029592804  //A

int analogVal = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("Serial ready.");
  
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);  

  analogReference(EXTERNAL);
}

void loop() {
   analogVal = analogRead(A0) - analogRead(A1);
   Serial.print("Digital Value: ");
   Serial.print(analogVal);

   Serial.print(" , Spannung an Shunt: [mV]: ");
   Serial.print(analogVal * voltageConversionFactor);

   Serial.print(" , Strom durch Shunt: [mA]: ");
   Serial.print(analogVal * shuntCurrentConversionFactor);

   Serial.print(" , Entsprechender Strom: [A]: ");
   Serial.println(analogVal*actualCurrentConversionFactor);

   delay(100);
}
