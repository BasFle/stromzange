<# Arduino Stromzange Beispiel
## Intro
Ziel ist es einen ersten Steckboard-Aufbau mit einem Arduino zur Messung eines Stromes mit der Stromzange [SCT-013](0-stromzange.md) umzusetzen.

## Schaltung
![](attachements/Pasted%20image%2020220718135040.png)
Shunt-Widerstand: 47 Ohm, da 33 nicht zur Verfügung stehen. Der Messbereich wird dadurch nach oben hin etwas limitiert.

Spannungsteiler: 470k + 470k mit 100µF Kondensator zur Stabilisierung

Die Stromzange liefert 0 - 50mA bei einem Eingang von 0 - 100A.
Dadurch ergibt sich:
$$U_{outmax} = 47\Omega *50mA = 2.35VRMS$$
$$U_{outPeak} = 2.35V*\sqrt{2} = 3.32 V$$
Wobei der Arduino durch das Anheben der Spannung auf 2.5V Mittelwert nur bis 2.5V messen kann.

## Umwandlung Digitalwert - Strom
Der ADC liefert einen 10 Bit digital Wert (D), wobei dieser auf 5V Referenz-Spannung (Uref) bezogen wird.
Daher gilt für die Spannung am Widerstand:
$$U_{Shunt}
= D * \frac{U_{ref}}{2^n}$$

Außerdem gilt der Zusammenhang:
$$U_{Shunt} = I_{Shunt}*R_{Shunt}$$

Durch Gleichsetzten folgt der Zusammenhang:
$$I_{Shunt} = D * \frac{U_{ref}}{2^n*R_{shunt}}$$

Die Stromzange liefert am Ausgang $I_{Shunt}$. wobei gilt:
$$I_{Shunt} = I_{measure} * \frac{50mA}{100A}$$
Also: Die Messzange liefert 0.5 mA pro A an zu messendem Strom.

Es gilt also weiter der Zusammenhang:
$$I_{measure} = D * \frac{U_{ref}*100A}{2^n*R_{shunt}*50mA}$$

Wobei es sich beim rechten Bruch um reine konstanten handelt, die somit in einen definierbaren Faktor.

Für Uref = 5V, 10Bit, 47 Ohm:
Faktor für Spannung am Shunt [mV] = 4.8828125
Faktor für Strom durch Shunt [mA] = 0.1038896
Faktor für zu messenden Strom [A] = 0.2077792553

Wobei jedoch in der Umwandlung der 2.5V Offset zu beachten ist. Daher: Man muss den gemessenen Spannungswert - 2.5V bzw. minus 512 zu rechnen ist.
$$I_{measure} = (D-512) * \frac{U_{ref}*100A}{2^n*R_{shunt}*50mA}$$
## Code
```
//Ref: 5V
//Bit: 10
//Rshunt : 47 R
//Stromübersetzung: 100A / 50mA 
// ->  0,0048828125 V / Bit
// -> 
#define voltageConversionFactor 4.8828125        //mV
#define shuntCurrentConversionFactor 0.1038896   //mA
#define actualCurrentConversionFactor 0.2077792  //A

int analogVal = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("Serial ready.");
  
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);  
}

void loop() {
   analogVal = analogRead(A0) - 512; //Minus 512 wegen 2.5V Offset
   Serial.print("Digital Value: ");
   Serial.print(analogVal);

   Serial.print(" , Spannung an Shunt: [mV]: ");
   Serial.print(analogVal * voltageConversionFactor);

   Serial.print(" , Strom durch Shunt: [mA]: ");
   Serial.print(analogVal * shuntCurrentConversionFactor);

   Serial.print(" , Entsprechender Strom: [A]: ");
   Serial.println(analogVal*actualCurrentConversionFactor);

   delay(1000);
}
```

## Test
### Ohne Stromzange
Um die Schaltungsfunktion, sowie den Code zu testen, wird im ersten Schritt anstelle der Stromzange ein Labornetzteil verbunden. Dort wird eine Spannung von 5V eingestellt. Die Strombegrenzung wird so variiert, dass ein Strom eingestellt werden kann. Das Labornetzteil dient als Stromquelle.

Es ist festzustellen, dass der ADC-Wert äußerst lange benötigt um sich einzupendeln/stabilisieren. Ursache ist vermutlich die Dimensionierung des Kondensators in Kombination mit den Widerständen für den Spannungsteiler.


### Resultate
Anschließend lässt sich ein Strom durch den Shunt messen, dieser Schwankt jedoch noch um einige Bits (+- 1 Bit).
Außerdem gibt es einen Offset, wenn kein Strom fließt.

Außerdem ist der Shunt-Widerstand selbst mit 47 Ohm äußerst gering definiert, wenn man bedenkt, dass nie bis 100A gemessen werden wird. Daher wird dieser Wert erhöht. Eine realisitische Dimensionierung liegt eher im Bereich von 350 Ohm (Messbereich bis ca. 10A). Enstprechend müssen die obigen Faktoren angepasst werden.

Außerdem wurde anstatt den digitalen Wert minus 512 zu rechnen, die differenzielle Spannung an den Eingängen A0 und A1 gemessen, wobei A1 am zweiten Anschluss des Shunts liegt.

### Mit Stromzange 
Im nächsten Schritt soll anstelle eines DC-Netzgerätes die Stromzange angeschlossen werden.

Als Gerät, welches unter Last gesetzt wird kommt eine Lampe zum Einsatz. Der Messaufbau mit den obigen Änderungen sieht daher wie folgt aus:
![](attachements/Pasted%20image%2020220719104041.png)

### Resultate
Es konnte kein Strom gemessen werden.

Als Test wurde an die Stromzange ein Ampermeter angeschlossen, auch damit wurde kein Strom gemessen

### Lösung
Die Stromzange darf nicht um das gesamte Versorgungskabel eines Geräts gewickelt werden, da die Summe der Ströme so null ist.

Daher: Man muss die Versorgung in L und N auftrennen. Dazu steht ein Adapter zur Verfügung. Eine der beiden Leitungen muss um die Stromzange gewickelt werden. 

### Ergebnisse
Bei ausgeschalteter Lampe wird keine Spannugnsdifferenz bzw. ein digitaler Wert von 1/-1 gemessen (leichte ungenauigkeit des ADCs des Arduino, siehe oberer Teil der nachfolgenden Werte.

Bei eingeschalteter Lampe werden digitale Werte im Bereich von 10 gemessen. Diese schwanken natürlich, da es sich um AC handelt.
![](attachements/Pasted%20image%2020220719104352.png)
Jedoch sieht man, dass die Spannung/Strom nicht berechnet werden. Dies liegt an einer falschen Definition der errechnung. Es wurde fälschlicherweise eine Integer Division durchgeführt.

Interessanter ist den Strom/Spannungsverlauf in einem Plot dargestellt zu sehen. Der Serial-Plotter des Arduino liefert:
Spannung:
![](attachements/Pasted%20image%2020220719105154.png)
Man sieht irgendwo ist ein Sinus o. Ä. vielleicht zu erkennen, jedoch eher schwierig :).

Daher: Wie viele Punkte messen wird überhaupt?

Mit millis() können wir ansehen, wie lange unser Programm läuft und mit wenigen Zeilen Code feststellen, dass pro Durchlauf, also Messung etwa 6ms vergehen, wenn wir nichts rechnen und nur die Werte einlesen.
Die bisherige Serielle Ausgabe mit allen Ausgaben verlängern dies auf über 120ms!

Ein begrenzender Faktor hierbei ist die Serial-Print Geschwindigkeit. Bisher liegt die Baud-Rate bei 9600, wobei neben den nötigen Zahlen viel unnötiger Text ausgegeben wird. 
Ohne den Text wird das ganze auf ca. 20ms verkürzt.
Nur die Messung und Rechnung selbst benötigt nur rund 7ms.

Die Serielle-Übertragungsgeschwindigkeit wurde daher auf das Maximumg (2000000) geändert. Somit lassen sich deutlich mehr Messungen erziehlen. Lässt man unnötigen Text weg, so werden die Dauern auf ca. 2ms reduziert. Bei 50Hz Netz lassen sich somit 10 Messungen pro Periode erzielen.

Mit dem Serial-Plotter lassen sich damit folgende Ergebnisse erzielen:
![](attachements/Pasted%20image%2020220719111930.png)

Nun können genügend Messwerte aufgezeichnet werden um für 50Hz relativ genau den RMS-Wert zu errechnen.
s