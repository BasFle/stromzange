[TOC]
## Vorgabe
Umgesetzt werden soll im Prinzip eine auf LoRa-Technologie basierende Stromzange. Die Daten sollen per Funkmodul auf das [The Things Network](https://eu1.cloud.thethings.network/console/applications/testapp12349182/data) hochgeladen werden.
Zusätzlich zur Stromzange sollen nach Möglichkeit noch weitere Sensoren an den µC angeschlossen werden. Auch diese Daten sollen mithilfe der LoRa Technologie auf das The Things Network geladen werden.

## Umsetzung
### Blockschaltbild
![](attachements/Pasted%20image%2020220801104155.png)
Oben dargestellt ist ein Blockschaltbild, des umgesetzten Systemes.

Wesentlich umgesetzt wurde dabei eine einfache Desktop-Applikation, welche Daten aus dem The Things Network bezieht, lokal anzeigt und in einer lokalen Datenbank abspeichert, sowie die Firmware für das STM32WL55, welches die Sensordaten einsammelt und an das Lora Netzwerk sendet.

Am einfachsten nachvollziehen lässt sich die Funktion, wenn man von der Stromzange SCT013 ausgeht und den Weg der Daten von dort aus verfolgt.

## Stromzange
[Datenblatt der Stromzange SCT-013](attachements/SCT-013-000-XiDiTechnology.pdf)
Die Stromzange wird um den Leiter, dessen Strom gemessen werden soll geschlossen. So kann kontaktlos der durch den Leiter fließende Strom ermittelt werden. In unserem Fall handelt es sich beim Wechselstrom um Strom, welcher vom Netz stammt, daher also auf einer Frequenz von 50 Hz basiert. 

**Hinweis**
Zu beachten ist, dass nur Wechselströme gemessen werden können. Außerdem kann die Stromzange nicht um ein klassisches 3-Adriges Kabel geschlossen werden, da in diesem Fall die "Summer der Ströme" 0 A entspricht, da sich sowohl Phase, als auch Neutralleiter innerhalb der Stromzange befinden.

Daher müssen diese Leiter durch einen Adapter voneinander getrennt werden. Ein möglicher Aufbau hierzu ist nachfolgend dargestellt:
![](attachements/Pasted%20image%2020220728141557.png)
Die Stromzange liefert in diesem Fall einen Ausgangsstrom, welcher proportional zum durch die Zange fließenden Strom ist. 100 A entsprechen dabei einem Ausgangsstrom von 50 mA, also einem Übersetzungsverhältnis von 2000 : 1. Da nur Wechselstrom gemessen werden kann, ergibt sich auch am Ausgang der Stromzange ein Wechselstrom.

Dieser Wechselstrom muss nun mit dem Mikrocontroller (STM32WL55) aufgezeichnet werden.
Dazu kommt folgende Strom/Spannungswandler Schaltung zum Einsatz:

![](attachements/Pasted%20image%2020220728143816.png)
An den Anschlüssen Input 1 und Input 2 wird dabei die Stromzange angeschlossen.

Der Strom fließt dabei über den Widerstand Rshunt, wodurch dort eine in entsprechendem Verhältnis stehende Spannung entsteht. Diese Spannung wird zu den Ausgängen Output 1 und Output 2 geführt. Die Differenzspannung dieser beiden Ausgänge steht im direkten Verhältnis zum Strom am Eingang. 

Die Widerstände R1 und R2 dienen als Spannungsteiler, welcher den Shunt-Widerstand auf ein DC-Potential von 1.65V (= Hälfte der Versorgungsspannung) hebt. Dadurch können sowohl die positiven, als auch negativen Halbwellen (Strom) gemessen werden.

### Dimensionierungshinweis
Der Shunt-Widerstand wurde auf 330 Ohm dimensioniert. Die Dimensionierung ist von der Versorgungsspannung (in unserem Fall 3.3V durch den µC), sowie den zu messenden Strombereich abhängig.

Für die Dimensionierung gilt: Es werden positive und negative Halbwellen gemessen, daher teilt sich der zur Verfügung stehende Spannungsbereich (3.3 V) durch zwei (1.65 V). Diese Spannung ergibt zusammen mit dem Shuntwiderstand den maximalen Shunt-Strom, welcher gemessen werden kann (I = U / R). In unserer Dimensionierung mit 330 Ohm ergeben sich 5mA, als Shunt-Strom. Da die Stromzange im Verhältnis 2000 : 1 übersetzt, kann somit ein maximaler Strom von 10 A gemessen werden.

**Achtung**
Die 10A beziehen sich auf den Spitzenwert des Stromes, der Effektivwert ist dabei nur 7.07A. Sollen größere Ströme gemessen werden, muss der Shunt-Widerstand geringer dimensioniert werden.

Ein größerer Widerstand reduziert den Messbereich, bietet jedoch den Vorteil, dass größere Spannungen entstehen, welche leichter weiterverarbeitet werden können.

## Ermittlung des Stromes
### Einzelne Messpunkte
Unabhängig vom Mikrocontroller wird am Ausgang die Differenzspannung mithilfe eines ADCs ermittelt.

Daraus kann relativ einfach auf den Strom, welcher im ursprünglichen Leiter fließt zurückgerechnet werden. Die nachfolgenden Rechnungen sind von folgenden Parametern abhängig:

```
m = 2000     //Übersetzungsverhältnis der Stromzange

Rshunt = 330 //Dimensioierung siehe oben

n = 12       //Auflösung des ADCs
Uref = 3.3   //Referenzspannung des ADCs
```

Aus dem Digitalwert "D" ergibt sich die Spannung am Shunt als:
$$U_{Shunt} = D * \frac{U_{ref}}{2^n}$$
Daraus folgt der Strom durch den Shunt:
$$I_{Shunt} = \frac{U_{Shunt}}{R_{Shunt}}$$
Daraus wiederum der Strom durch den ursprünglichen Leiter:
$$I_{mess}=I_{Shunt}*m$$
Die Formeln lassen sich zu folgendem Zusammenhang zwischen digitalem Messwert und Strom zusammenfassen:
$$I_{mess}=D*\frac{U_{ref}*m}{R_{Shunt}*2^n}$$
Zu erwähnen ist, dass es sich bei allem, mit ausnahme von D und dem Strom um konstanten handelt. Für die Umrechnung, können diese Werte also zu einem einzelnen, konstanten Faktor zusammengefasst werden.

### Effektivwert
Oben dargestellt ist, wie einzelne Messwerte errechnet werden können. Jedoch handelt es sich tatsächlich, wie bereits erwähnt um Wechselstrom, daher schwankt der obige Wert andauernd. Um auf Leistung, o.Ä. zurückschließen zu können ist es daher nötig, den Effektivwert zu berechnen.

Der Effektivwert beschreibt jenen DC-Strom, bei dem die selbe Leistung übertragen wird, wie beim entsprechenden Wechselstrom.

Um den Effektivwert zu ermitteln stehen grundsätzlich zwei Möglichkeiten zur Verfügung:

1. Idealer Sinus
	Trifft man die Annahme, dass der fließende Strom eine Sinus-Signalform aufweist, wie dies die Netzspannung tut, gilt für den Effektivwert des Signales der einfache Zusammenhang: $I_{Eff}=\frac{I_{Spitze}}{\sqrt(2)}$ 
	Weicht die Signalform jedoch von einem Sinus ab, stimmt diese Rechnung nicht mehr. Daher wird die zweite Möglichkeit zur Bestimmung angewendet.

2. True-RMS
	Auf Multimetern liest man hierzu häufig "True-RMS", dabei wird nicht von einem Sinus ausgegangen, sondern der wirkliche Effektivwert errechnet bzw. messtechnisch bestimmt.
	Für den Effektivwert gilt die Formel: $$I_{eff}=\sqrt{\frac{\sum{I^2}}{n}}$$
	Um diese Berechnung anzuwenden müssen jedoch genügent Messpunkte gesammelt werden, um ein genaues Ergebnis zu erzielen.

Genaueres zur angewendeten Berechnung folgt noch.

## LoRa
Der oben ermittelte Strom soll später mit Hilfe von LoRa übertragen werden. Wichtige Grundlagen sind unter [13-LoRa](13-LoRa.md) zusammengefasst.

Für unsere Programmierung kommt wie oben erwähnt das STM32WL55 zum Einsatz. Dieses wird mithilfe der STMCubeIDE programmiert (für mich sehr neu, ungewohnt, deutlich komplizierter als das was ich aus der Schule gewohnt bin). Als Vorlage wird das Beispielprojekt "LoRa End Node", welches aus nachfolgender Datei zu entnehnmen ist verwendet [How to build a lora application with STM32Cube](attachements/an5406-how-to-build-a-lora-application-with-stm32cubewl-stmicroelectronics.pdf).

Diese hat das Senden von Daten mit LoRa im Prinzip bereits implimentiert, muss also nur noch verstanden und angepasst werden.

### Kommunikationsparameter für die Verbindung
Für die LoRa Verbindung selbst wurden folgende Kommunikationsparameter eingestellt:
```
DeviceEUI = 00, 80, E1, 15, 00, 0A, 98, F4
Application Key = 2B,7E,15,16,28,AE,D2,A6,AB,F7,15,88,09,CF,4F,C3
Nework Key = 2B,7E,15,16,28,AE,D2,A6,AB,F7,15,88,09,CF,4F,3C
App/Join EUI = 21, 21, 21, 21, 21, 21, 21, 21
Nework Session Key = 2B,7E,15,16,28,AE,D2,A6,AB,F7,15,88,09,CF,4F,3C
Application Session Key = 2B,7E,15,16,28,AE,D2,A6,AB,F7,15,88,09,CF,4F,3C
```
Wichtig ist dabei die DeviceEUI, diese ist auf dem Board selbst als Sticker aufgebracht. Sie identifiziert das Gerät selbst.
Die restlichen Daten können im Prinzip beliebig gewählt werden, sie müssen jedoch mit der Applikation im The Things Network übereinstimmen.

Als End-Stelle wird auf dem The Things Network eine neue Applikation erstellt. Diese trägt in diesem Fall den Namen "Testapp". Dort wird ein neues Gerät hinzugefügt. Die Daten werden dort gleich wie oben eingestellt. 

**Hinweis**
Die wichtigsten Einstellungen welche beim Hinzufügen getroffen wurden sind:
```
Frequency plan = Europe863-870 MHz (SF9 for RX2 - recommended)
LoRaWAN version = 1.0.3
Regional Parameters version = RP001 Regional Parameters 1.0.3 revision A
```
Wichtig ist, dass auch im STM32-LoRa Programm die Version 1.0.3 ausgewählt werden muss. Standardmäßig ist diese auf 1.0.4 eingestellt, was jedoch zu Problemen geführt hat.

Im Live-Data-Tab können die Daten, welche vom Endgerät empfangen werden betrachtet werden. Für jedes Datenpaket gibt es einen "Payload", das ist jener Teil, der später Sensordaten, ... enthalten kann. Übertragen werden jedoch nur einzelne Bytes, die Daten müssen selbstständig codiert/entcodiert werden.

### Wichtigste Programmfunktionen
Das bereits exisiterende End Node Template, hat bereits alle Funktionen zum Senden von Daten bzw. Empfangen implimentiert. Der gesamte LoRa-Ablauf wird über die Datei "lora_app.c" gesteuert.
Die entscheidendsten Funktionen sind hierbei:

`SendTxData` - Diese Funktion wird, wenn eine Verbindung mit einem LoRa Gateway hergestellt wurde alle 10 Sekunden aufgerufen. Dort werden die Daten übertragen.

Entscheidend ist hierbei "AppData.Buffer". Es handelts ich dabei um ein Byte-Array, welches den oben erwähnten Payload = die Daten enthält. Diese Funktion wurde so angepasst, dass die gewünschten Daten übertragen werden.

`OnRxData` - Diese Funktion wird immer aufgerufen, wenn unser LoRa Gateway Daten empfängt.

Nun aber noch ein kurzer Ausflug in die beschaffung der Daten, welche wir übertragen wollen.

## STM32 Datenerfassung
Die Datenübertragung erfolgt alle 10 Sekunden.
Übertragen werden dabei jeweils:

### RMS-Wert des Stromes
Um diesen zu ermitteln müssen Messpunkte gesammelt werden. Diese sollten im ms Bereich aufgezeichnet werden. Aus den Messpunkten, wird vor jeder Datenübertragung, nach der Rechnung von oben der ADC der Effektivwert des Stromes, welcher in dieser Zeit geflossen ist ermittelt. Verwendet werden dabei die ADC-Eingänge 1 und 3, welche PB14 und PB4 des Boards bzw. die Anschlüsse "A4" sowie "A3" darstellen.

Zum Messen im ms Abstand bietet sich die Verwendung eines Timers an. Das LoRa Programm, welches verwendet wird funktioniert jedoch in kombination mit den Timern, welche das STM32 zur Verfügung stellt nicht.

Das LoRa Programm selbst bietet jedoch auch interne Timer an. Nachteil ist, dass hier die minimale Periodendauer auf 3ms beschränkt ist. Für unser Programm wurde dies jedoch als ausreichend empfunden. Es wird also alle 3ms ein Messereignis ausgelöst.
In einem Messereignis werden mithilfe der Methoden `SYS_GetChanel3Value` und `SYS_GetChanel1Value`, welche programmiert wurden, die ADC-Werte eingelesen. Anhand dieser kann der Strom, welcher in diesem Moment geflossen ist ermittelt werden.

#### Problem
Man könnte nun für jeden Punkt den Strom berechnen und diesen als float in ein Array abspeichern. Nach 10 Sekunden (wenn die Daten übertragen werden), wird aus allen Messpunkten der Effektivwert berechnet und hochgeladen.
Das Problem hierbei ist jedoch dass sich bei 10 Sekunden und einer Messdauer von 3 ms, ~3300 Floats ergeben, welche gespeichert werden müssen. Was etwa 105.6kBit (32Bit/float) entspricht, also äußerst inneffektiv ist.

Man könnte alternativ über 10 Sekunden die digitalen Werte der ADCs abspeichern (int16) und beim hochladen den jeweiligen Strom und dann den Effektivwert zu errechnen, man benötigt jedoch immernoch  52.8kBit an speicher.

#### Lösung
Betrachtet man die Formel für den Effektivwert, welche verwendet wird:
$$RMS = \sqrt{\frac{\sum{I²}}{n}}$$
So ist zu erkennen, dass es ausreicht bei jedem Timer-Event den Strom zu berechnen, das Quadrat daraus zu bilden und zur Summe der bisherigen Messwerte hinzuzuzählen.
Im Hintergrund läuft ein Zähler mit, welcher zählt, wie viele Messungen gemacht wurden.

Alle 10 Sekunden, wenn die Werte hochgeladen werden sollen, wird dann die aktuelle Summe durch die Anzahl an Messpunkten dividiert, sowie die Wurzel daraus gebildet. Dies entspricht dann dem RMS-Wert der letzten 10 Sekunden.

Um die Summe zu bilden wird eine double verwendet. Hochgeladen wird am Ende eine Float-Zahl. So müssen nicht alle Messwerte unnötig abgespeichert werden.

Hochgeladen werden dabei die einzelnen 4-Bytes der Float, welche auf dem Server wieder zusammengefügt werden können.

Auf 10 Sekunden kommen jeweils etwa 3400 Messwerte, welche von den ADCs gemessen werden um den Effektivwert zu bilden.

### Temperatur
Das Board selbst verfügt bereits über einen internen Temperatursensor, welcher sehr einfach über die Methode `EnvSensors_Read(&sensor_data);` ausgelesen werden kann.

### RTC
An das Board wurde per I2C-Schnittstelle eine RTC angeschlossen.
Zum Einsatz kommt das RTC-Modul DS1307-V03 [Datenblatt des RTC-Bausteins](attachements/datasheet.pdf).

Letzendlich kommt eine Library dafür zum Einsatz: https://github.com/eepj/DS1307_for_STM32_HAL/

Verwendet wird die I2C1 Schnittstelle des Boards. SDA ist dabei PA10, SCL PA9.

Da es nicht wirklich viel Sinn ergibt die Uhrzeit mit hochzuladen, da dies onehin im Web-Interface genauer ermittelt und angezeigt wird, kommt die RTC nicht wirklich zum Einsatz. Sie wird nur noch verwendet, damit sie nicht umsonst implimentiert wurde :).
Es wird beim Starten eines neuen 10 Sekunden Zykluses bis zum Hochladen von Daten die Sekunden-Zeit ausgelesen und beim Ende des Zykluses. Es ergibt sich zwar eigentlich immer eine Differenz von 10 Sekunden, jedoch wird diese als "Durration" mit den Daten hochgeladen.

### Luftfeuchtigkeit
Die Luftfeuchtigkeit wird durch den Sensor DHT11 ermittelt. Dieser liefert einen Seriellen Output. Er wird an PA1 angeschlossen.

Auch hier kommt eine Libary zum Einsatz:
[How to use DHT11 sensor with STM32 » ControllersTech](https://controllerstech.com/using-dht11-sensor-with-stm32/)

Hinweis: Aktuell funktioniert der Sensor nicht und gibt nur Dummy-Daten aus.

## Datenformatierung
Um kurz nochmals festzuhalten. Ermittelt und hochgeladen werden sollen:
1. RMS-Strom welcher über 10 Sekunden ermittelt wurde (float)
2. Anzahld der Messpunkte, welche dafür nötig waren (uint32)
3. Dauer des Ganzen in Sekunden (uint8)
4. Temperatur (uint8)
5. Luftfeuchtigkeit (uint8)

Die Daten, welche man hochladen möchte werden einfach in den `AppData.Buffer[]` = Byte Array gespeichert.
Alle anderen Zeilen, welche dort Dinge hineinspeichern wurden auskommentiert, daher: es werden nur unsere Daten hochgeladen. Um mit diesen wieder etwas anfangen zu können, muss fix festgelegt sein, welches Byte, welchen Daten entspricht:

---

==Payload-Bytes==
Byte 0 - Dauer über die der RMS ermittelt wurde in Sekunden
Byte 1 - Temperatur in °C
Byte 2 - Luftfeuchtigkeit in %
Byte 3 - RMSStrom - float Byte 4
Byte 4 - RMSStrom - float Byte 3
Byte 5 - RMSStrom - float Byte 2
Byte 6 - RMSStrom - float Byte 1
Byte 7 - Anzahl der Messpunkte - uint32 Byte 1
Byte 8 - Anzahl der Messpunkte - uint32 Byte 2
Byte 9 - Anzahl der Messpunkte - uint32 Byte 3
Byte 10 - Anzahl der Messpunkte - uint32 Byte 4

---

Byte 0 - 2 sind dabei eben einfach die entsprechenden uint8 Zahlen, ganz hochgeladen.
Byte 3 - 6 bilden die Float, welche den Effektivwert des Stromes darstellt. Die Float besteht aus 4-Bytes, welche einzeln hochgeladen werden. Byte 4 ist dabei die obersten 8 Bit, ...
![](attachements/Pasted%20image%2020220728165110.png)
Byte 7 - 8 bilden die uint16, welche der Anzahl an Messpunkten, zur Ermittlung des Stromes entspricht, Byte 1 ist dabei das untere Byte, Byte 2 das obere

Konkret sieht das ganze im C-Code wie folgt aus:
```C
AppData.Buffer[i++] = endSecond-StartSecond;
		  AppData.Buffer[i++] = (uint8_t)sensor_data.temperature;
		  AppData.Buffer[i++] = (uint8_t)80;

		  if(rmsValue > 0.05)
		  {
			  unsigned char floatBytes[4] = {0};

			  union {
				  float a;
				  unsigned char bytes[4];
				} thing;
				thing.a = rmsValue;
				memcpy(floatBytes, thing.bytes, 4);

			  AppData.Buffer[i++] = floatBytes[3];
			  AppData.Buffer[i++] = floatBytes[2];
			  AppData.Buffer[i++] = floatBytes[1];
			  AppData.Buffer[i++] = floatBytes[0];
		  }else
		  {
			  AppData.Buffer[i++] = 0;
			  AppData.Buffer[i++] = 0;
			  AppData.Buffer[i++] = 0;
			  AppData.Buffer[i++] = 0;
		  }

		  uint8_t bytes[2];
		  memcpy(bytes, &ADCMeasurementCounter, sizeof(ADCMeasurementCounter));
		  AppData.Buffer[i++] = bytes[0];
		  AppData.Buffer[i++] = bytes[1];
```
Wobei den größten Teil des Codes das Umwandeln der Float in Bytes ausmacht. Werte die unter 50mA sind, werden auf 0 abgerundet. Die Stromzange eignet sich zum messen dieser onehin nicht. So wird z.B. auch verhindert das 1e-36, ... (floats halt) hochgeladen wird.

In der Applikation auf TheThingsStack erhalten wird als Payload nun z.B. folgende Daten:
`0x0A,0x1A,0x50,0x3F,0x15,0x5B,0x58,0x54,0x0D`
Es handelt sich um unsere 8-Byte in hex-Darstellung.

Wir können einen Payload-Formtter angeben, welcher aus diesen Daten wieder die ursprünglichen Werte errechnet. Dieser sieht für unsere Applikation wie folgt aus:

```JavaScript
function Decoder(bytes, port)
{
  var decoded = {};

decoded.measDurration = bytes[0];
decoded.measTemp = bytes[1];
decoded.measHumidity = bytes[2];

var rmsCurrent = decodeFloat32(bytes[6]| (bytes[5]<<8)|(bytes[4]<<16)|bytes[3]<<24);
decoded.rmsCurrent = rmsCurrent;

var measPointsForRMS = bytes[7]|(bytes[8]<<8);
decoded.measPointsForRMS = measPointsForRMS;

return decoded;
}

function decodeFloat32(bytes)
{
    var sign = (bytes & 0x80000000) ? -1 : 1;
    var exponent = ((bytes >> 23) & 0xFF) - 127;
    var significand = (bytes & ~(-1 << 23));

    if (exponent == 128)
        return sign * ((significand) ? Number.NaN : Number.POSITIVE_INFINITY);

    if (exponent == -127) {
        if (significand == 0) return sign * 0.0;
        exponent = -126;
        significand /= (1 << 22);
    } else significand = (significand | (1 << 23)) / (1 << 23);

    return sign * significand * Math.pow(2, exponent);
}

```

In LiveData erhalten wir so den Output: 
![](attachements/Pasted%20image%2020220728165537.png)
Unsere Daten wurden hochgeladen und dekodiert.

## Datenverarbeitung mit TheThingsNetwork
Die Daten die wir hochladen wollen wir mit dem TheThingsNework jetzt natürlich irgendwie speichern, anzeigen, ...

Lädt man die Daten einfach hoch, werden einem diese in LiveData angezeigt. Macht man das Browser Fenster zu, werden die Daten jedoch gelöscht.
Es gibt jedoch die Möglichkeit sich über Webhooks, MQTT, ... die Daten zu besorgen/zu verarbeiten.
Angewendet wurden zwei Dinge:

- Storage Integration
- Webhook

### Storage Integration
Storage Integration muss einfach aktiviert werden. Dann werden alle Daten, die man ansonsten in Live data angezeigt bekommen würde, hier in einer Datenbank abgespeichert. Auf die Datenbank kann später per API zugegriffen werden.

**Nachteil**
Die Daten werden in der kostenlosen Version des The Things Network nur für eine begrenzte Zeit abgespeichert. (30 Tage, 1 Woche oder 24 Stunden, tatsächlich bin ich mir nicht sicher?).

Die Storage Integration wird von der Website jedoch nicht für das Herunterladen von aktuellen Daten (sprich: polling) empfohlen, sondern um sich alte Daten zu holen, da die Daten vom Empfangen bis zum abspeichern in der Datenbank eine gewisse Zeit brauchen können.

### Webhook
Es lässt sich zustätzlich oder alternativ eine Webhook einrichten, an die die Empfangenen Daten weitergeleitet werden. Es gibt kostenlose Webhooks Hosts im Internet (z.B. https://webhook.site/), die bis zu 500 Datenpakete annehmen, auf welche ebenfalls wiederum per API zugegriffen ewrden kann. Die Webhooks eigenen sich dazu, aktuelle Daten zu erhalten, da die Daten beim Empfang direkt an diese weitergeleitet werden.


Beides wurde eingerichtet und wird nachfolgend auch verwendet.
Die Daten sehen dabei jeweils wie in etwa wie folgt aus (bei der Webhook gibt es davor noch einen Header):
Es handelt sich um JSON-Daten, wobei jede Uplink-Nachricht so abgespeichert wird. Besonders interessant ist für uns eigentlich nur die Uhrzeit, wann die Daten gepushed wurden (revieced_at) (Achtung: UTC, 2h hinter unserer Zeit), sowie der "decoded_payload", welcher unsere Daten enthält. Der rest ist im Prinzip hauptsächlich Informationen über die Verbindung.

```JSON
{
  "end_device_ids": {
    "device_id": "eui-0080e115000a98f4",
    "application_ids": {
      "application_id": "testapp12349182"
    },
    "dev_eui": "0080E115000A98F4",
    "join_eui": "2121212121212121",
    "dev_addr": "260BF1CF"
  },
  "correlation_ids": [
    "as:up:01G94ACV8VMV2EQGAT70DBQ94W",
    "gs:conn:01G8CFD5WQP1MFSXKF0VD8TTD6",
    "gs:up:host:01G8CFD5XR9TC9867B33RTPWW4",
    "gs:uplink:01G94ACV289WHYX8E0FXP1CTSZ",
    "ns:uplink:01G94ACV286XXCN65SBCS5WMXS",
    "rpc:/ttn.lorawan.v3.GsNs/HandleUplink:01G94ACV2885J8ZZ45KTVG44GN",
    "rpc:/ttn.lorawan.v3.NsAs/HandleUplink:01G94ACV8T6TYSPWH34RJC427V"
  ],
  "received_at": "2022-07-29T06:30:42.715408072Z",
  "uplink_message": {
    "session_key_id": "AYJIlXmZ1beBNI4gdctPTQ==",
    "f_port": 2,
    "f_cnt": 110,
    "frm_payload": "ChstPxUsQVQN",
    "decoded_payload": {
      "measDurration": 10,
      "measHumidity": 45,
      "measPointsForRMS": 3412,
      "measTemp": 27,
      "rmsCurrent": 0.5827065110206604
    },
    "rx_metadata": [
      {
        "gateway_ids": {
          "gateway_id": "gantner-solutions-v1",
          "eui": "A84041217F604150"
        },
        "time": "2022-07-29T06:30:42.483843Z",
        "timestamp": 437525788,
        "rssi": -37,
        "channel_rssi": -37,
        "snr": 10.2,
        "location": {
          "latitude": 47.079163956660565,
          "longitude": 9.911644756793976,
          "source": "SOURCE_REGISTRY"
        },
        "uplink_token": "CiIKIAoUZ2FudG5lci1zb2x1dGlvbnMtdjESCKhAQSF/YEFQEJy60NABGgwIkv2NlwYQ1cmj8AEg4OrG9N3nsgEqDAiS/Y2XBhC4t9vmAQ==",
        "channel_index": 7
      }
    ],
    "settings": {
      "data_rate": {
        "lora": {
          "bandwidth": 125000,
          "spreading_factor": 7
        }
      },
      "coding_rate": "4/5",
      "frequency": "867900000",
      "timestamp": 437525788,
      "time": "2022-07-29T06:30:42.483843Z"
    },
    "received_at": "2022-07-29T06:30:42.504774689Z",
    "consumed_airtime": "0.056576s",
    "network_ids": {
      "net_id": "000013",
      "tenant_id": "ttn",
      "cluster_id": "eu1",
      "cluster_address": "eu1.cloud.thethings.network"
    }
  }
}
```

## Was tun mit den Daten?
Es gibt also 2 Datenquellen: Langfristigen Speicher und aktuelle Webhook Daten
Um diese Daten darstellen zu können wurde eine Desktop Applikation realisiert. Diese hat einerseits die Aufgabe, die Daten aus den 2 Quellen zu sammeln und andererseits die Aufgabe, die Daten visuell darzustellen.

### Aufbau der Desktop-Applikation
Die Applikation basiert auf C#, es handelt sich um eine WPF-Anwendung.

Die Applikation greift per API auf die Storage Integration und den Webhook host zu, um die Daten einzusammeln. 
Außerdem wird beim 1. Starten der Applikation ein Speicherort ausgewählt (kann geändert werden). An diesem wird vom Programm eine SQLite-Datenbank erstellt (im Prinzip einfach nur eine Datei). Dort werden die Daten, welche von der Applikation gesammelt werden längerfristig offline abgespeichert.
So löst sich einerseits das Problem, dass das TheThingsNetwork die Daten nur begrenzt speichert (wenn die Applikation regelmäßig ausgeführt wird) und der Zugriff auf das Web ist nur für "neue Daten" nötig, nichtmehr für Daten, welche bereits heruntergeladen wurden.

Die Daten, welche von den APIs geholt werden, werden auf die relevanten Payload-Daten, sowie die Uhrzeit gefiltert/herunter gekürzt. Intern wird das Ganze in einer Klasse "CurrentMeasurementPoint" gespeichert. Für jeden Datenpunkt wird ein Objekt erstellt. Darin wird Uhrzeit der Messung, Dauer, Luftfeuchtigkeit, Temperatur, RMS-Strom und noch Daten für die Applikation selbst, abgespeichert. Außerdem gibt es eine Klasse "CurrentMeasurementsList", welche das Verwalten aller Datenpunkte (Speichern/Öffnen/...) ermöglicht.
 ![](attachements/Pasted%20image%2020220729085447.png) ![](attachements/Pasted%20image%2020220729085442.png)     

### Aufbau des UIs
Das UI bietet 3-Tabs an.

#### Datagrid
Dieser Tab enthält, wie der name sagt einfach ein Datagrid, mit allen Datenpunkten, die aktuell geladen sind. Es kann nach den einzelnen Spalten sortiert werden.

![](attachements/Pasted%20image%2020220803154204.png)

#### Plot
Dieser Tab ermöglicht es die Messpunkte als Graph darstellen zu lassen.
![](attachements/Pasted%20image%2020220803154218.png)
Links oben kann der anzuzeigende Zeitraum ausgewählt werden.

Oben kann ausgewählt werden, welche Plots man sich anzeigen lassen möchte.
Mit einem Slider kann eingestellt werden, dass jeweils 1-50 Datenpunkte zu einem Plot-Punkt zusammengefasst werden -> dadurch werden nicht unnötig viele Datenpunkte geplottet.

Das ganze wird als Graph dargestellt. Bei Hover mit der Maus über den Graphen wird einem der zugehörige Datenwert angezeigt. Jede größe erhält ihre eigene Achse.

#### Downlink and Save
Das 3. Fenster ermöglicht es Nachrichten an das Board zu senden bzw. die Messpunkte, welche geladen sind in einer Datei abzuspeichern/den Speicherort welcher verwendet wird zu ändern. Auch kann eine bereits gespeicherte Datei geöffnet werden oder die Applikation versteckt werden, dann wird sie nur noch im System Tray angezeigt und fetched alle paar Minuten einmal die neusten Daten. Ansonsten macht sie nichts mehr.

Es ist möglich, die drei auf dem Board verbauten LEDs zu steuern, einzustellen, was auf dem eingebauten 7-Segment Display angezeigt wird (Gewünschte Zahl, Uhrzeit, Gemessener Strom, Aus). Außerdem kann die Periodendauer, mit welcher das Board Daten hochlädt eingestellt werden.
![](attachements/Pasted%20image%2020220803154239.png)
Dies geschieht durch senden einer Downlink-Nachricht an das Board. Die Applikation kann dies durch Zugriff auf eine API tun.
Das Board wird beim Empfangen von Daten die Funktion `OnRxData` aufgerufen. Dort werden die Empfangenen Daten verarbeitet.

## Downlink Datenformate
Das Datenformat für das Downlink wurde wie folgt festgelegt:

**LEDs**
- Es wird Port 5 angesprochen
- Es wird ein Byte übertragen, wobei die unteren 3 Bits den Status von LED1-3 repräsentieren. "0" ist dabei aus, "1" ist dabei ein.
- Das 4. Bit ist ein Flag, welches wenn dies gesetzt ist alle LEDs ausschaltet
- Das 5. Bit ist ein Flag, welches wenn dies gesetzt ist alle LEDs einschaltet (das Aus Bit ist hoherwertig, wenn beide gesetzt sind)

**7-Segment anzeige**
- Es wird Port 6 angesprochen
- Es werden 5 Bytes übertragen, wobei Byte 1 den aktuell angesprochenen Modus angibt (0 = Off, 1 = Zahl, 2 = Zeit, 3 = Strom)
- Die weiteren 4 Bytes geben die Zahl an, welche, sovern dieser Modus ausgewählt wurde übertragen wird. Ist ein anderer Modus ausgewählt, wird die Zahl weiterhin übertragen

**Ändern der Übertragunsrate**
- Es wird Port 7 angesprochen
- Es werden 2 Bytes übertragen, welches den Wert für die Übertragunsrate in Sekunden enthält (Byte 1 = höherwertiger uint16 Teil, Byte 2 = niederwertigerer uint16 Teil)
- Erlaubt sind Zeitwerte von 10 Sekunden bis 7200 Sekunden (2 Stunden)

### Wie fetched die Anwendung Messdaten?
#### Prinzip
Insgesamt haben wir praktisch 3 Datenquellen. 
- Die Datenbank, welche lokal gespeichert wurde und sämtliche Daten enthält, die bereits in vorherigen Sessions heruntergeladen wurden.
- Die Storage Integration Cloud, welche langfristig die empfangenen Daten speichert
- Der Webhook, welcher es ermöglicht aktuelle Daten zu beziehen

Der Ablauf, mit welchem unser Programm ab dem Starten Daten sammelt ist dabei so:
![](attachements/Pasted%20image%2020220729094554.png)
1. Es werden lokale Daten geladen
2. Alle in der Storage Integration gespeicherten, neuen Daten werden bezogen
3. Alle in der Webhook gespeicherten, neuen Daten werden bezogen
4. Alle Webhook Daten werden gelöscht (weil dort nur 500 Pakete gespeichert werden können, und die alten Daten onehin in der Storage Integration liegen)
5. Alle 10 Sekunden wird die Webhook auf neue Daten überprüft und diese lokal abgespeichert

So werden stets die aktuellen Daten des Sensors bezogen, ohne, dass es durch die verschiedenen Datenquellen zu Duplikaten kommt. Lokal werden die Daten wie gesagt in einer SQLite Datenbank gespeichert.
![](attachements/Pasted%20image%2020220729094843.png)

#### Storage Integration API Zugriff
Hierzu wird ein Key benötigt, welcher in der Applikation auf The Things Network hinzugefügt werden muss.
Das C#-Snippet für den Zugriff, welcher alle Daten nach einem gewünschten Datum liefert sieht dabei wie folgt aus:
```C#
   string key = "NNSXS.W7A5J4NQ4JUGED6B24WCXGVRONHQCTHGENJGCHA.FDPR4XRISCJEFY7M4ABZF3YXDYR5T4IPYGO4POKHGAE62CV47YDQ";

            CurrentMeasurementPoint latestExisitingPoint = GetLatestMeasurement();
            DateTime utcTime = latestExisitingPoint.Stop.ToUniversalTime();

            HttpResponse<string> response = Unirest.get($"https://eu1.cloud.thethings.network/api/v3/as/applications/testapp12349182/packages/storage/uplink_message?after={utcTime:yyyy-MM-dd}T{utcTime:HH:mm:ss}Z")
                .header("Authorization", $"Bearer {key}")
               .asJson<string>();

            string web = response.Body.ToString();
```
Web enthält nun die JSON-Daten, aller Uplink-Messages, welche nach dem gewünschten Timestamp hochgeladen wurden.
Es wird auf UTC umgerechnet, da die Datenbank darauf arbeitet, lokal jedoch mit der lokalen Zeit gearbeitet wird.

Aus diesen JSON-Daten können durch String-Operationen (Trimmen, Teilen, ...) die Daten für die Messpunkte ermittelt werden.

#### Webhook API Zugriff
Dieser sieht im Prinzip genau gleich aus wie der obige Zugriff.
```C#
 HttpResponse<string> response = Unirest.get("https://webhook.site/token/a22ce7b7-2634-41be-9860-988ec38432b7/requests?sorting=newest")
            .header("Accept", "application/json")
        //    .header("per_page ", "15")
            .asJson<string>();

            string json = response.Body.ToString();
```

Um alle bisherigen Daten zu löschen dient folgender Zugriff:
```C#
            HttpResponse<string> response = Unirest.delete("https://webhook.site/token/a22ce7b7-2634-41be-9860-988ec38432b7/request")
            .header("Accept", "application/json")
            .asJson<string>();
```

#### Downlink Nachrichten per API senden
Hierzu dient folgender Zugriff für die LEDs:
```C#
string key = "NNSXS.W7A5J4NQ4JUGED6B24WCXGVRONHQCTHGENJGCHA.FDPR4XRISCJEFY7M4ABZF3YXDYR5T4IPYGO4POKHGAE62CV47YDQ";

            StringBuilder sb = new StringBuilder();
            sb.Append("{\"downlinks\":[{\"decoded_payload\":{");
            sb.Append($"\"led1\":\"{cbLED1.IsChecked}\",");
            sb.Append($"\"led2\":\"{cbLED2.IsChecked}\",");
            sb.Append($"\"led3\":\"{cbLED3.IsChecked}\",");

            bool allOn = false;
            bool allOff = false;
            if(sender as Button != null)
            {
                Button tmp = sender as Button;
                if(tmp.Tag.ToString() == "alloff")
                {
                    allOff = true;
                }else if(tmp.Tag.ToString() == "allon")
                {
                    allOn = true;
                }
            }

            sb.Append($"\"allOff\":\"{allOff}\",");
            sb.Append($"\"allOn\":\"{allOn}\"");
            sb.Append("},\"f_port\":5");
            sb.Append("}]}");

            HttpResponse<string> response = Unirest.post($"https://eu1.cloud.thethings.network/api/v3/as/applications/testapp12349182/devices/eui-0080e115000a98f4/down/replace")
                .header("Content-Type:", "application/json")
                .header("Authorization", $"Bearer {key}")
                .body(sb.ToString())
               .asJson<string>();

            string web = response.Body.ToString();
```

Der Zugriff auf die 7-Segment anzeige sieht gleich aus, f_port wird auf 6 geändert und im payload wird number1, number2, number3, number4 anstelle von den LEDs übertragen.

Welche LEDs dabei ein/ausgeschaltet sind, sowohl auch ob alle LEDs aus-/eingeschaltet werden sollen wird dabei in einer JSON als "True" bzw. "False" angegeben, auf Seite vom The Things Network, werden diese Daten wiederum in ein Byte umgerechnet. Hierzu dient der Downlink-Payload-Formatter (nachfolgend nur exemplarisch, der tatsächliche Formatter sieht mitlerweile etwas anders aus):
```JavaScript
function encodeDownlink(input)
{
  // Input is a string true or false
  var bytes = [];
  
  if(input.fPort == 5){
  bytes[0] = 0;
  
  if(input.data.led1 == "True")
  {
    bytes[0] |= 0x01;
  }
  
  if(input.data.led2 == "True")
  {
    bytes[0] |= 0x02;  
  }
  
  if(input.data.led3 == "True")
  {
    bytes[0] |= 0x04;  
  }
  
  if(input.data.allOff == "True"){
    bytes[0] |= 0x08;
  }
  
  if(input.data.allOn == "True"){
    bytes[0] |= 0x10;
  }
  }
  else if(input.fPort == 6)
  {
    bytes[0] = input.data.number1;
    bytes[1] = input.data.number2;
    bytes[2] = input.data.number3;
    bytes[3] = input.data.number4;
  }
  
  return {
    bytes: bytes,
    fPort: input.fPort
  };
}

function decodeDownlink(input) {
  
  if(input.fPort == 5){
      return {
    data: 
    {
      led1: ["False", "True"][input.bytes[0]&0x01],
      led2: ["False", "True"][input.bytes[0]&0x02],
      led3: ["False", "True"][input.bytes[0]&0x04],
      allOff: ["False", "true"][input.bytes[0]&0x08],
      allOn: ["False", "true"][input.bytes[0]&0x10]
    }
    };
  }
  else if(input.fPort == 6)
  {
      return {
      data:
      {
        number1:input.bytes[0],
        number2:input.bytes[1],
        number3:input.bytes[2],
        number4:input.bytes[3]
        }
    };
  }
}
```

## Header aufbau
Der gesamte Aufbau wurde auf eine Lochraster Platine gelötet. Diese ist nachfolgend zu sehen. Alle größeren Dinge (Stromzange, RTC, Umwelt, Display, STM32WL55) werden einfach aufgesteckt. 
Jeder Sensor kann theoretisch auch weggelassen werden, dann werden die Messwerte schlicht mit 0 ersetzt bzw. ergeben sich als solches.

**Header ohne Bauteile**
![](attachements/Pasted%20image%2020220802133649.png)

**Header mit Bauteilen**
![](attachements/Pasted%20image%2020220802133157.png)

## Tests
### 1. Genauigkeit der Strommessung
Hierzu wurde zusätzlich zur Strommessung zusätzlich zu unserer Stromzange ein Multimeter (Agilent U1252B) verwendet und das Kabel mit verschiedenen Stromflüssen belastet, die gerade zur Verfügung standen.

| Digital MM | Stromzange | Last           |
| ---------- | ---------- | -------------- |
| 0 A        | 0 A        | Keine          |
| 0.09 A     | 0.09 A     | Lötkolben idle |
| 0.13 A     | 0.13 A     | Lampe          |
| 0.5 A      | 0.5 A      | Bildschirm     |

Der Test ist nicht ausführlich. Jedoch liefert die Stromzange weitgehend den gleichen Ausgabewert wie das angeschlossene Multimeter. Es ist darauf zu achten, dass die Messung nur dann genaue Messwerte liefert, wenn die Stromzange richtig geschlossen wird.

### 2. LoRa Verbindungsreichweite
Theoretisch sollte LoRa eine sehr große Reichweite bieten (km-Bereich). Doch wie gut funktioniert die Verbindung?
Der Test ist recht simpel. Der Praktikant :) nimmt seinen Aufbau mit in den Zug, wo dieser per USB mit Strom versorgt wird. Auf dem Notebook kann man sich die Daten anschauen und nachvollziehen, bis wo/ab wo/... eine Verbindung bestanden hat.