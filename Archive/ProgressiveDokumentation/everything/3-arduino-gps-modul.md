## GPS-Modul
Um den Einsatz-Ort der Zange zu prüfen, wird ein GPS-Modul an den Arduino angeschlossen.
Verwendetes Modul: GY-GPS6MVZ

Achtung: Benötigt einen Spannungsteiler auf Rx, da hier nur 3.6V erlaubt sind.
Library: [TinyGPS++ | Arduiniana](http://arduiniana.org/libraries/tinygpsplus/)
Verbindung: Rx über Spannungsteiler (4.7k und 10k ) auf Pin 3,Tx auf Pin 4

### The TinyGPS++ Object Model

The main TinyGPS++ object contains several core sub-objects:

-   **location** – the latest position fix
-   **date** – the latest date fix (UT)
-   **time** – the latest time fix (UT)
-   **speed** – current ground speed
-   **course** – current ground course
-   **altitude** – latest altitude fix
-   **satellites** – the number of visible, participating satellites
-   **hdop** – horizontal diminution of precision

Objekt erstellen:
`TinyGPSPlus gps;`

Problem: Empfängt kein GPS-Signal
