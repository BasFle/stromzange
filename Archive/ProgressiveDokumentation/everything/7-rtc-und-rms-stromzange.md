Die Stromzange misst dauernd den fließenden Strom.

Der Arduino misst diesen laut  [2-arduino-stromzange](2-arduino-stromzange.md).

Im Hintergrund gibt es hierbei 2 Arrays. Ein Array behält die letzten x-Werte (100) im Speicher. Das Array wird mit Werten befüllt und die Summe dabei mitgebildet. Bei jeder neuen Messung, wird ein Wert aus der Liste aktualisiert, sowie die Summe angepasst, dass sie mit neuen Wert übereinstimmt:

```C
for(int i = 0; i<rmsArrayLength; i++)
  {
   curSum -= curValues[i];  //Alter Wert wird von Summe abgezogen

	//Neuer Wert wird ermittelt:
   analogVal = (analogRead(A0) - analogRead(A1));
   
	if(analogVal >= -1 && analogVal <= 1)
	   {
	      analogVal = 0;  //Removes Measurements which are -1/1, often occur when turned off.
	   }

	//Neuer Wert wird in Array gespeichert und ersetzt den alten
   curValues[i] = analogVal*analogVal*actualCurrentConversionFactorSquared/windungen;
 
   curSum += curValues[i]; //Neuer Wert wird zu Summe hinzugefügt

   while((millis() - prevMillis) < 2){ } //Es wird alle 2ms eine Messung durchgeführt, so werden je 10 Messungen/Periode der 50Hz Netzfrequenz durchgeführt, was für eine relativ genaue RMS-Ermittlung ausreicht

   prevMillis = millis();  
   }
```

Jedes mal wenn hier 100-Werte eingelesen wurden (alle 200ms), wird der aktuelle Stand in ein zweites Array kopiert. Dieses hat y-Werte (300). Es dient dazu, Werte zu sammeln, welche später der Durchschnittsbildung für ein Hochladen des Stromverbrauchs dienen.

Die RTC überprüft hierbei immer, ob eine volle Minute (Sekunde = 0) vergangen ist. Ist dies der Fall, wird der Durchschnitt des zweiten Arrays auf den Server hochgeladen oder ausgegeben.

```C
for(int a = 0; a<MaxRmsPerPush; a++){
  for(int i = 0; i<rmsArrayLength; i++)
  {
   curSum -= curValues[i];
   
   analogVal = (analogRead(A0) - analogRead(A1));
   
   if(analogVal >= -1 && analogVal <= 1)
   {
      analogVal = 0;  //Removes Measurements which are -1/1, often occur when turned off.
   }
   
   curValues[i] = analogVal*analogVal*actualCurrentConversionFactorSquared/windungen;
 
   curSum += curValues[i];

   //if(i%measurementsPerOutput == 0){
     //Serial.print("Current True-RMS: ");
   //Serial.println(sqrt(curSum*1.00/rmsArrayLength),3);
   //}

   while((millis() - prevMillis) < 2){ }

   prevMillis = millis();  
   }
   pushSum += sqrt(curSum*1.00/rmsArrayLength);
 //  Serial.print(a);
  // Serial.print("\t");
  // Serial.println(pushSum);

   rtc.getTime();

   if(rtc.second == 0 && a > 10)
   {   
     Push(a);  
    
     break;
   }
 }
}
```

Also einfach gesagt:
-> Es wird jeweils der Durchschnitt über 10 Perioden (200ms) gebildet und gespeichert.
-> Aus diesen Durschnitten wird alle X-Sekunden der Durchschnitt gebildet und die Push-Funktion aufrufen. Dazu wird jeweils die aktuelle RTC-Zeit abgefragt

Die Stromzange funktioniert soweit.
Die Leistung kann durch den RMS Strom und die eigentlich konstanten 230V RMS einfach als I * P berechnet werden.
