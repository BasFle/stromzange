# Stromzange SCT-013
Datenblatt: 
Input: 0-100A
Output: 0-50mA
              0.5mA/A

Output auf ~2.5V heben (10k/470k/...?)
Dimensionierung - Shunt: 33 $\Omega$ 
Quelle: [SCT013 - Current sensor (domoticx.com)](https://domoticx.com/sct013-current-sensor/#:~:text=The%20SCT013%20sensors%20are%20current%20transformers%2C%20instrumentation%20devices,electrical%20equipment%20without%20having%20to%20cut%20it%20off.)

Genauigkeit: 1-2%, **Achtung**: Nur bei korrekter Anwengung (vollständige Schließung der Zange)
