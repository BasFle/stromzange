Der STM32 verfügt zwar über eine integrierte RTC, jedoch muss das Gerät dazu über eine externe Batterie versorgt werden.
Daher wird einfach das bereits am Arduino angeschlossene DS1307 Modul verwendet.

Das Modul wird über die I2C-Schnittstelle des STM32 angesprochen. Die Zeit wurde bereits auf dem Arduino darauf konfiguriert.

Verwendet wird die I2C1 Schnittstelle des Boards. SDA ist dabei PA10, SCL PA9.

Die I2C-Adresse des Bausteines ist: 0xD0
[Datenblatt des RTC-Bausteins](attachements/datasheet.pdf)

Angewendet wird I2C dabei relativ einfach: [STM32 I2C Master using HAL (embeddedexplorer.com)](https://embeddedexplorer.com/stm32-i2c-master-tutorial/#:~:text=STM32%20I2C%20Master%20using%20HAL%20I%202%20C,clock%20line%20%28SCL%29%20and%20one%20data%20line%20%28SDA%29.)

Letzendlich kommt eine Library für I2C zum Einsatz: https://github.com/eepj/DS1307_for_STM32_HAL/
Diese ermöglicht es die Zeit, etc. sehr einfach einzulesen bzw. auch zu setzten.

Probleme hat gebracht, dass das RTC Modul nicht erkannt wurde. Vermutlich wurden falsche I2C-Pins ausgewählt bzw. verwendet.