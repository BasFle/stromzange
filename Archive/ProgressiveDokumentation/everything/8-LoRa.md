# Intro
## Was ist Lora?
Lora = Long Range Wide Area
Low Power, long Range (bis 9km) Netzwerk Technologie

## LoRa vs LoraWAN
LoRa beschreibt die Modulationstechnologie, welche hohe Reichweite bei geringem Leistungsverbrauch ermöglicht (Physikalischer Layer)
LoRaWAN beschreibt ein Netzwerkprotokoll, welches LoRa Chips für die Kommunikation verwendet (MAC Layer)

## Frequenzband
- Europe 867 - 869MHz
- North America 902 - 928MHz
- China 470 - 510MHz
- Korea/Japan 920 - 925MHz
- India 865 - 867MHz

# Wie funktioniert LoRa?
Eine Karte über die verbundenen Gateways: [The Things Network](https://www.thethingsnetwork.org/map)

## Prinzip
Befindet sich ein LoRa Gerät in Reichweite eines Gateways, verbindet es sich damit. Über das Gateway werden die Daten weitergeleitet und dem entsprechenden Nutzer bzw. der entsprechenden Applikation zugeordnet. So ist es möglich, dass ein LoRa Gerät quasi überall funktionieren kann, solange ein Gateway in Reichweite ist. In Vorarlberg beispielsweise im gesamten Reintal.

![](attachements/Pasted%20image%2020220720081413.png)

## Device EUI, Join EUI, App Key
Diese 3-Parameter beschreiben jedes LoRaWAN fähige Gerät. 

DevEUI - Eindeutige 64-Bit Geräteadresse (wird von IEEE vergeben)
JoinEUI - Eindeutige 64-Bit Adresse, wleche dem Applikationsserver zugeordnet ist (auch hier IEEE vergeben
AppKey - 128-Bit Verschlüsselung zwischen Nachricht und Applikationsserver, muss für jedes Gerät eindeutig sein

## The Things Network
www.thethingsnetwork.org, stellt ein Netzwerk für Geräte, Applikationsserver, ... zur Verfügung.

# LoRa Einstieg
Ein guter Einstieg in LoRa, der relativ anfängerfreundlich ist und gutes Verständnis bringen kann ist das Loris Base-Board. Es ist quasi Plug and Play und bringt Verständnis: [Loris Base.pdf](attachements/LorisBase.pdf)
Ein Nachteil des Boards ist, dass es einen ST-Link-Programmer benötigt, um programmiert werden zu können.

Ein weiteres Board, welches verwendet werden kann und über deutlich mehr Möglichkeiten verfügt ist das STM32WL55 Board. Hier wird keine ST-Link benötigt.

# Programmierung
Beide der oben genannten Boards kommen mit einigen Beispielen zur Programmierung. 
Entscheidend ist, jeweils die Daten des LoRa Boards im Code einzutragen.
Nachfolgend wird über das STM32WL55-Board gesprochen:
![](attachements/Pasted%20image%2020220720134118.jpg)

Hierfür muss die Datei: "se-identity.h" angepasst werden.
Entscheidend sind:
- Device EUI
- Join EUI
- Device-Adresse
- NWK-Key

Hat man diese Daten eingetragen, kann man den Code auf das Board spielen. Über Tera-Term kann eine Serielle Verbindung hergestellt werden, um Outputs vom Board zu erhalten.

Wichtig ist auch in TTN die richtigen Einstellungen zu treffen. Im Fall des STM32WL55 hat vorallem die Netzwerk einstellung Probleme bereitet. Tutorials laufen auf einer veralteten Version bzw. im Amerikanischen Netzwerk, weshalb diese Einstellung dort nicht gezeigt wird.

---

[How to build a lora application with STM32Cube](attachements/an5406-how-to-build-a-lora-application-with-stm32cubewl-stmicroelectronics.pdf)

Aus obiger Datei lässt sich entnehmen, dass jediglich "TxEventType_t" in "lora_app.c" auf TX_ON_EVENT geändert werden muss, damit das Hochladen nicht mehr alle X-Sekunden geschieht, sondern bei einem Event (standardmäßig bei Button1).

Dazu wird durch den Button ein Interrupt ausgelöst. Nach diesem wird die Task zum Senden von Daten ausgelöst.
Dazu dient die Zeile:
`UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_LoRaSendOnTxTimerOrButtonEvent), CFG_SEQ_Prio_0);`

Das Auslösen dieses Events wurde als Funktion "SheldueTransmision" hinzugefügt. Durch aufrufen dieser Funktion, kann nun ein Sende-Event ausgelöst werden.

---

## Ablauf der zu Verbindung führt
1.) Device mit LoRaWAN Version 1.1 erstellen
2.) Device sollte zu ständigem Accept-join-request/Accept-join-message werden (diese Anfragen werden dauernd gesendet, jedoch kommt es zu keinem Datenaustausch)
3.) Jetzt unter General Settings > Network Settings die LoRaWAN Version auf 1.0.3 ändern

