Aus [2-arduino-stromzange](2-arduino-stromzange.md) erhalten wir viele Messwerte. Aus diesen sollte nach Möglichkeit der RMS-Wert des Stromes errechnet werden.

### Möglichkeit 1 - Peak-Peak
Trifft man die Annahme, dass es sich um einen idealen Sinus handelt, lässt sich aus dem Spitze-Spitze-Wert einfach der RMS-Wert errechnen:
$$U_{rms} = \frac{U_{peak}}{\sqrt(2)}$$

Dies ist jedoch ungenau, sobald die Stromform von einem Sinus abweicht, was je nach Last (induktiv/kapazitiv/...) vorkommen kann.

### Möglichkeit 2 - True RMS
Der RMS ergibt sich rechnerisch tatsächlich als:
$$\sqrt{\frac{\sum{V ^2}}{n}}$$
Daher: Alle Spannungen werden addiert und durch die Anzahl geteilt.

Im Code lässt sich dies mit einem Array umsetzten. Die Summe kann dabei bestmöglich fortlaufend gebildet werden.


## Code
Da in unserem Fall die Lampe eine sehr Sinusförmige Stromaufnahme aufweist, ist wird dies als Kontrolle der Berechnung des True-RMS verwendet.

```C
for(int i = 0; i<rmsArrayLength; i++)
  {
   curSum -= curValues[i];
   analogVal = (analogRead(A0) - analogRead(A1));
   if(analogVal >= -1 && analogVal <= 1)
   {
      analogVal = 0;  //Removes Measurements which are -1/1, often occur when turned off.
   }

   analogVal = analogVal/2;
   
   curValues[i] = analogVal*analogVal*actualCurrentConversionFactorSquared;
   analogValues[i] = analogVal;
   curSum += curValues[i];

   Serial.print("Current True-RMS: ");
   Serial.println(sqrt(curSum*1.00/rmsArrayLength));

   while((millis() - prevMillis) < 2){
    
   }

    prevMillis = millis();  
   }
     
    int maxVal = 0;
    
    for(int i = 0; i<rmsArrayLength; i++)
    {
      if(analogValues[i] > maxVal)
      {
        maxVal = analogValues[i];
      }
    }
    Serial.print("By max:");
    Serial.println(maxVal*actualCurrentConversionFactor/sqrt(2));

```

Es ergibt sich das Problem, dass der RMS nicht immer über eine Periode gebildet wird. Eine Möglichkeit ist es, dafür zu sorgen, dass die Dauer der Messung mit 50Hz übereinstimmt. Daher: Immer genau 2ms, so sind 10 Messungen pro Periode möglich.
Eine weitere Möglichkeit ist es, das Array zu verlängern, dadurch wird die Schankung geringer.

Der Vergleich zwischen errechnung mit Maximalwert und RMS zeigt, dass die Ergebnisse identisch sind.
In diesem Fall liefert das ganze 0.1 bzw. 0.11A.

Die Messung mit dem Multimeter ergibt 0.13A. Es gibt also noch einen Messfehler.
Ändert man die Last auf einen größeren Verbrauch: 0.422 A, ergibt sich bei uns: 0.34A, also auch hier eine Abweichung.
