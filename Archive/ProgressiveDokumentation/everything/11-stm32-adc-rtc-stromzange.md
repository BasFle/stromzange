Die Teile Funktionieren nach den obigen Punkten bereits einzeln. Auch die Stromzange funktioniert auf dem Arduino bereits. Nun werden diese Teile zu einem zusammengefügt. Bzw. der Code vom Arduino auf den STM32 transportiert.

Probleme haben sich dadurch ergeben, dass der LoRaWAN Prozess die Interrupts der Timer des STM32 blockiert. Zwar liefert das Modul die Möglichkeit Timer zu initialisieren, jedoch beträgt dabei die minimale Periodendauer 3ms. Bei 20ms Periodendauer von 50Hz Netzfrequenz, ergeben sich so nur 6-7 Abtastungen / Periode.

### Ursprüngliche Idee
Da die Timer nicht anders umgesetzt werden konnten, wird das ganze soweit akzeptiert. Beachtet man, dass der Durchschnittswert onehin über viele Perioden, dauerhaft gebildet werden soll, ist es onehin ausreichend, 6-7 Messwerte pro Periode zu erhalten.
Im Gegenteil, theoretisch ist es vermutlich sogar vorteilhaft, wenn der Durchschnitt über mehrere Perioden gebildet wird, da dadurch nicht immer die gleichen Messüunkte auf einem Verlauf abgetastet werden, sondern sich die Punkte über die Periode bewegen.
Durch 3ms Abtastrate, kommen auf 3 ganze Perioden des Netzes, insgesamt 20 Abtastpunkte.

Parallel zum Timer, welcher die ADC-Messungen alle 3ms durchführt und in ein Array abspeichert, läuft ein Timer mit einer Periodendauer von 1 Sekunde. Bei auslösen dessen Timer-Events, wird jeweils der RMS-Wert der 3ms-ADC-Messungen gebildet.
Diese RMS-Werte werden in einem anderen Array gesammelt. Das Array mit den bisherigen Werten wird gelöscht und laut dem 3ms-ADC-Timer wieder neu befüllt. SO wird verhindert, dass extrem viele ADC-Messergebnisse gesammelt werden müssen.

Alle 10 Sekunden wird ein Upload-Event ausgelöst. Dort wird dann der **durchschnitt** aus dem RMS-Werten der bisherigen Messungen gebildet und mit Start und Stop Zeit hochgeladen.

Die 10 Sekunden für Upload-Zeit sind als `APP_TX_DUTYCYCLE` konstante definiert
Die 1 Sekunde, nach welcher jeweils der RMS errechnet wird ist als `CurrentAverageCalculationPerdiod` definiert

Die Array-Längen sind als `ADC_Measurement_Array_Length` und `CurrentAverageArrayLength` definiert, wobei diese groß genug dimensioniert sein müssen. Wird ein Array zu klein definiert, wird beim Erreichen des Endes der erste Wert wieder überschrieben.

#### Problem
Tatsächlich ist das bilden aus den Durchschnitten der RMS-Werte falsch, da dies nicht mehr dem RMS-Wert entspricht. Ein Beispiel:
Unter der Annahme, dass die Messwerte 10,20,30,40,50,60 sind, ergibt sich ein RMS-Wert von: 38,94
Errechnet man die RMS-Werte von 10,20,30 (21,60) und 40,50,60 (50,66) und bildet aus diesen beiden Werten den Durchschnitt ergibt sich: 36,13, was nicht dem wirklichen RMS-Wert entspricht. Auch bei errechnung des RMS-Wertes mit diesen beiden Werten, ist das Ergebnis etwas verfälscht.

In unserer Messung ist der Einfluss recht gering, da sich der RMS-Wert zwischen den Perioden kaum ändert, daher ist der Durchschnitt der RMS-Werte quasi gleich wie der wirkliche RMS-Wert. Jedoch ist die rechnung faktisch falsch, weshalb der Ansatz etwas verändert wurde.

### Lösung
a.) Man speichert über die gesamten 10-Sekunden alle ADC-Messwerte und bildet dann daraus den RMS-Wert.
Jedoch benötigt man dann ein int16 Array von einer Länge von > 3400 > 54.4kBit, was deutlich über dem aktuellen Speicherverbrauch von einem int16-Array der Länge 350 sowie einem float (32-Bit) der Länge 15 = 6kBit liegt. 
Zu erwähnen ist, dass bei der aktuellen Methode dafür der Rechenaufwand höher ist.

b.) Man speichert nicht den RMS-Strom-Wert:
$$RMS = \sqrt{\frac{\sum{I²}}{n}}$$
Sondern die Summe der einzelnen Messwerte quadriert (SUM(I²)), parallel dazu speichert man die Anzahl der zugehörigen Messwerte in einem zweiten Array mit (n). Anhand dieser Speicherdaten kann bei Hochladen dann der richtige RMS-Wert errechnet werden. 
Der Speicheraufwand ist dabei etwas größer als bisher: bisheriger aufwand + int16 array von der selben Länge, wie die Float-Werte = 16 * 20 = 320 Bit, also minimal.

Also: Aus der Formel oben wird einmal $\sum{I²}$ und n gespeichert.
Beim Hochladen können dann alle n-s und alle Summen zusammengezählt werden. Diese dividiert man und wir ziehen die Wurzel. Die RMS-Rechnung ist so korrekt.

Theoretisch kann aus dem 12-Bit ADC-Wert auch noch ein 8-Bit Wert errechnet werden, so könnte man theoretisch die Hälfte der Bits für die ADC-Werte einsparen, man würde jedoch etwas an Genauigkeit verlieren.