Wie  [4-rms-errechnen](4-rms-errechnen.md) zeigt, weichen unsere Messungen von den Messungen mit dem Multimeter ab.
Konkret ist der Messaufbau:
![](attachements/Pasted%20image%2020220719131357.png)
Die Last besteht aus Oszilloskop, bzw. Oszilloskop + Lampe und einem kleinen Netzteil.
|               | Oszi + Lampe  | Nur Oszi      |
| ------------- | ------------- | ------------- |
| Agilent U125B | 0.4235 A      | 0.302 A       |
| Stromzange    | 0.368-0.381 A | 0.265-0.275 A |
| Abweichung    | 0.04-0.05 A   | 0.027-0.03A   |


Mögliche Ursachen für die Abweichung sind:

#### Wert des Shunt-Widerstandes
Der Shunt-Widerstand wird mit 330 Ohm angenommen. Tatsächlich hat dieser mit dem Multimeter gemessen einen Widerstandwert von 328.7 Ohm.

Nach korrektur dieses Wertes ergibt sich keine nennenswerte Änderung.

#### Referenzspannung des ADCs
Die Referenzspannung kann als Quelle der Abweichung ausgeschlossen werden, da diese mit dem Multimeter auf 4.99V gemessen werden kann, also äußerst genau ist.

### Windungen
Als großer Fehler konnte nun festgestellt werden, dass 2 Windungen durch die Stromzange zu führen nicht ideal ist bzw. diese groß genug sein 
müssen. Führt man den Draht nur noch 1x durch die Stromzange lassen sich deutlich bessere Messergebnisse erzielen.

