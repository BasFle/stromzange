## Code
```
//Ref: 5V
//Bit: 10
// ->  0,0048828125 V / Bit
#define conversionFactor 4.8828125

int analogVal = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("Serial ready.");
  
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);  
}

void loop() {
   analogVal = analogRead(A0);
   Serial.print("Digital Value: ");
   Serial.print(analogVal);

   Serial.print(" , Voltage [mV]: ");
   Serial.println(analogVal * conversionFactor);

   delay(1000);
}
```

## Output
![](attachements/Pasted%20image%2020220718131244.png)

Funktioniert im Prinzip: genauigkeit: Fraglich