Was, wann gemacht wurde.

## 18.07.22
Zuerst wurde eine Einführung in die eigentliche Problemstellung gegeben.

Als erste Aufgabe wurde festgelegt das Konzept mithilfe eines Arduinos umzusetzten. Daher: Die Daten der Stromzange mit einem Arduino messen und verarbeiten. Erst wenn dies Funktioniert wird das ganze mit LoRa in Verbindung gebracht.

1. [1-arduino-ADC](1-arduino-ADC.md) - Im ersten Schritt wurde ein simpler ADC mit Arduino in Betrieb genommen, um das Wissen darüber wieder aufzufrischen.
2. [2-arduino-stromzange](2-arduino-stromzange.md) - Im zweiten Schritt wird die Stromzange mit einer Schaltung verbunden, welche es ermöglicht über einen Shunt-Widerstand anhand der Spannung den tatsächlichen Widerstandswert zu ermitteln.