Um das Projekt vom Arduino auf den STM32 zu emigrieren wird dort als erstes der ADC programmiert.

- 12 Bit
- Right/Left alligned (es bietet sich rechts an, da mit 12-bit gearbeitet wird)
- Cont-Conversion-Mode
- ADON in ADC_CR2 register setzten um Continious Mode zu starten
- EOC Flag wird bei Ende gesetzt und bei gesetztem JEOCIE Bit wird ein Interrupt ausgelöst
- Externe Referenz die an Vdda angelegt wird (3.3V in unserem Fall)

ADC funktioniert soweit.
Kann über:
`SYS_GetChanel3Value` bzw. `SYS_GetChanel1Value` aufgerufen werden und liefert einen uint16-12-Bit-Wert zurück.

AUf dem Board entspricht Chanel1 = PB14 = A4, Chanel3 = PB4 = A3.

